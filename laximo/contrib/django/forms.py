# coding=utf-8


import copy
import inspect
import logging
import re
from collections import OrderedDict

from django import forms
from django.core.validators import RegexValidator, MinLengthValidator
from django.utils.translation import ugettext_lazy as _

from laximo import objects
from laximo.oem import query as oem_query

logger = logging.getLogger(__name__)

_(u'Model code')
_(u'Model')
_(u'Frame')
_(u'Date')
_(u'Model Name')
_(u'Market')
_(u'Engine')
_(u'Transmission')


FIELDS_KWARGS = {
    'vin': {
        'label': _(u'VIN'),
        'help_text': _(
            u'Please enter VIN (17 symbols), example: %(example)s'
        ),
        'validators': [MinLengthValidator(17)]
    },
    'frame': {
        'label': _(u'Frame code'),
        'required': False,
        'widget': forms.HiddenInput(),
    },
    'frameno': {
        'label': _(u'Frame number'),
        'required': False,
        'widget': forms.HiddenInput(),
    },
    'frame_full': {
        'label': _(u'Frame No'),
        'required': True,
        'help_text': _(
            u'Please enter frame code and number, example: %(example)s'
        ),
    }
}


class CatalogOperationField(forms.CharField):
    def __init__(self, *args, **kwargs):
        super(CatalogOperationField, self).__init__(*args, **kwargs)


class QueryForm(forms.Form):
    """
    Базовый класс формы
    """

    class Meta(object):
        pass

    description = ''

    ssd = forms.CharField(
        initial='',
        widget=forms.HiddenInput(),
        required=False,
    )

    @property
    def name(self):
        return self.__class__.__name__


class FrameSearchForm(QueryForm):
    """
    Базовый класс формы поиска по номеру кузова
    """

    def clean_frame_full(self):
        frame_full = self.cleaned_data.pop('frame_full', '').strip()

        if not frame_full:
            raise forms.ValidationError("Укажите номер кузова")

        frame_full = re.sub('(\s+)|(\-+)', '-', frame_full)

        try:
            frame_code, frame_no = frame_full.split('-')
        except ValueError:
            raise forms.ValidationError(
                "Некорректный номер кузова: код и номер кузова должны быть "
                "разделены пробелом или дефисом"
            )

        self.cleaned_data['frameno'] = frame_no.strip()
        self.cleaned_data['frame'] = frame_code.strip()

        return self.cleaned_data


class BaseFormFactory(object):
    """
    Базовый класс фабрики форм
    """

    def create(self):
        raise NotImplementedError


class QueryFormFactory(object):
    """
    Фабрика форм для запроса к сервису Laximo (подбор авто по параметрам, поиск
    по VIN, etc.)

    Созданием полей управляют следующие методы:
        _build_query_args_fields():
            поля, необходимые для построения запроса, полученные из
            ```query_cls``` посредством метода ```get_query_args()```
        _build_instance_fields():
            если в метод ```create()``` передан экземпляр объекта и для фабрики
            определен ```fields_builder```, то в этом методе создаются поля
            для данного экземпляра (GetWizardForm, etc.)

    Attributes:
        form_cls:
            базовый класс создаваемой формы
        query_cls:
            класс запроса в Laximo (oem.CatalogInfo, etc.)
        fields_builder:
            класс, который из экземпляра объекта (```laximo.objects```),
            переданного в метод ```create()``` создает поля формы
        extra:
            kwargs для инициализации полей, словарь, где ключи - имена полей
            (аргументы для инициализации объекта Query), значения - словари для
             передачи в инициализатор поля
    """

    def __init__(self, query_cls, fields_builder, form_cls=QueryForm,
                 exclude_query_args=None, extra=None):
        assert issubclass(fields_builder, BaseFieldsBuilder)
        self.form_cls = form_cls
        self.query_cls = query_cls
        self.exclude = self._get_exclude(exclude_query_args)
        self.extra_kwargs = extra or copy.deepcopy(FIELDS_KWARGS)
        self.fields_builder = fields_builder(self)

    @staticmethod
    def _get_exclude(exclude):
        if not exclude:
            exclude = []
        elif isinstance(exclude, str):
            exclude = [s.strip() for s in exclude.split(',')]
        else:
            exclude = list(exclude)

        return exclude

    @property
    def _classname(self):
        return self.query_cls.__name__ + str('Form')

    def create(self, instance=None):
        """
        Создает форму
        :return:
        """
        fields = self.fields_builder.build(instance)
        form = type(self.form_cls)(self._classname, (self.form_cls,), fields)
        return form

    def get_query_args(self):
        """
        Берет аргументы инициализатора объекта Query (oem.CatalogInfo, etc.)
        из свойства ```self.query_cls``` и возвращает кортеж из двух
        элементов, где первый - все доступные поля, второй - обязательные поля
        :return:
        """
        query_args = inspect.getargspec(self.query_cls.__init__)

        field_names = [arg for arg in query_args.args if arg != 'self']

        if query_args.defaults:
            num_required = len(field_names) - len(query_args.defaults)
        else:
            num_required = len(field_names)

        fields_required = field_names[0:num_required]

        return field_names, fields_required


class BaseFieldsBuilder(object):
    """
    Базовый класс построителя полей

    Сначала создаются поля, соответствующие инициализатору объекта
    Query (```self.factory.query_cls```)

    Затем, если в метод ```build()``` передан экземпляр, создаются
    поля экземпляра.

    Логика создания полей должна быть описана в методе
    ```build_instance_field()```, который должен возвращать словарь, ключи
    которого - имена полей, значения - сами поля
    """
    def __init__(self, factory):
        self.factory = factory

    def get_query_arg_field_kwargs(self, field_name, instance):
        """
        Возваращет kwargs для создания поля
        :param field_name:
        :param instance:
        :return:
        """
        kwargs = self.factory.extra_kwargs.get(field_name, {})
        description = getattr(instance, 'description', None)
        if 'help_text' not in kwargs and description:
            kwargs['help_text'] = _(description)
        return kwargs

    def build_query_arg_field(self, field_name, required=False, instance=None):
        """
        Создает поле
        :param field_name:
        :param required:
        :param instance:
        :return:
        """
        field_kwargs = self.get_query_arg_field_kwargs(field_name, instance)
        required = field_kwargs.pop('required', required)
        return forms.CharField(required=required, **field_kwargs)

    def build_query_args_fields(self, instance=None):
        """
        Создает поля для аргументов инициализатора соответствующего
        объекта Query
        :param instance:
        :return:
        """
        fields = {}
        field_names, required = self.factory.get_query_args()
        field_names = [name for name in field_names if name not in self.factory.exclude]

        for field_name in field_names:
            fields[field_name] = self.build_query_arg_field(
                field_name, field_name in required, instance=instance
            )

        return fields

    def build_instance_fields(self, instance):
        """
        Stub
        :param instance:
        :return:
        """
        return {}

    def build(self, instance):
        """
        Создает поля
        :param instance:
        :return:
        """
        fields = self.build_query_args_fields(instance)
        instance_fields = self.build_instance_fields(instance)
        fields.update(instance_fields)
        return fields


class WizardFieldsBuilder(BaseFieldsBuilder):
    """
    Создает поля формы для подбора авто по параметрам (Wizard)
    из объекта ```laximo.objects.Wizard```
    """

    # todo test this class

    # Атрибуты объекта WizardParameter, значение которых
    # необходимо установить в HTML data-attrs у Choice-поля
    DATA_ATTRIBUTES = [
        'allowlistvehicles',
        'determined',
        'automatic',
    ]

    @staticmethod
    def choices_from_parameter(parameter):
        """
        Создает choices из параметра объекта Wizard
        :param parameter:
        :return:
        """
        choices = [['', '']]
        choices.extend([o.key, o.value] for o in parameter.options)

        if parameter.determined and len(choices) == 1:
            # todo описать зачем так делается
            choices[0][0] = parameter.ssd
            choices.extend([['', parameter.value]])

        return choices

    @staticmethod
    def update_data_attrs(field, parameter):
        """
        Проставляет необходимые data-атрибуты для поля
        :param field:
        :param parameter:
        :return:
        """
        for name in WizardFieldsBuilder.DATA_ATTRIBUTES:
            val = getattr(parameter, name, None)
            if val is None:
                continue
            attr_name = "data-%s" % name
            field.widget.attrs[attr_name] = getattr(parameter, name)

    def field_from_parameter(self, parameter):
        """
        Создает поле из параметра объекта Wizard
        Один параметр - одно Choice-поле для запроса в сервис
        :param parameter:
        :return:
        """
        choices = self.choices_from_parameter(parameter)

        field_kwargs = {
            'required': False,
            'choices': choices,
            'label': _(parameter.name),
            # 'help_text': ''
        }
        field = forms.ChoiceField(**field_kwargs)
        self.update_data_attrs(field, parameter)

        return parameter.name, field

    def build_instance_fields(self, instance):
        """
        Создает поля формы из экземпляра объекта Wizard
        :param instance:
        :return:
        """
        if not instance:
            return {}

        parameters = getattr(instance, 'parameters', [])

        fields = OrderedDict(
            list(map(self.field_from_parameter, parameters))
        )
        return fields


class FeatureFieldsBuilder(BaseFieldsBuilder):
    """
    Построитель полей для CatalogFeature
    """
    def get_query_arg_field_kwargs(self, field_name, instance):
        """
        Создает поля из аргументов инициализатора.

        Если передан аргумент ```instance```, то пытаемся отформатировать
        help_text, используя значения из ```instance.example``````
        :param field_name:
        :param instance:
        :return:
        """
        kwargs = super(FeatureFieldsBuilder, self).get_query_arg_field_kwargs(
            field_name, instance
        )

        # вставим example в help_text, если он определен
        if hasattr(instance, 'example') and 'help_text' in kwargs:
            kwargs['help_text'] = kwargs['help_text'] % {
                'example': _(instance.example)
            }

        return kwargs


class OperationFieldsBuilder(BaseFieldsBuilder):
    """
    Построитель полей для CatalogOperation
    """

    @staticmethod
    def build_operation_field(instance):
        """
        Создает поле формы для свойства CatalogOperation.operation
        :param instance:
        :return:
        """
        field = forms.CharField(
            required=True,
            widget=forms.HiddenInput(),
            initial=instance.name,
        )
        return field

    def build_instance_field(self, instance):
        """
        Создает поле формы для свойства CatalogOperation.field
        :param instance:
        :return:
        """
        regex_validator = RegexValidator(instance.pattern)
        description = getattr(instance, 'description', '')

        field_kwargs = {
            'required': True,
            'validators': [regex_validator],
            'label': _(instance.description),
            'help_text': _(description),
        }
        field = forms.CharField(**field_kwargs)
        field.widget.attrs['data-regexp'] = instance.pattern
        return instance.name, field

    def build_instance_fields(self, instance):
        """
        Создает поля формы для экземпляра CatalogOperation, переданный через
        параметр ```instance```
        :param instance: CatalogOperation
        :return:
        """
        if not instance:
            return {}

        fields = OrderedDict(list(map(self.build_instance_field, instance.fields)))
        fields['operation'] = self.build_operation_field(instance)
        return fields


class FilterFieldsBuilder(BaseFieldsBuilder):
    """
    Построитель полей для фильтров (oem.GetFilterByUnit|GetFilterByDetail)
    """
    @staticmethod
    def build_filter_choices(filt):
        choices = [('', '')]
        choices.extend((v.ssdmodification, v.name) for v in filt.values)
        return choices

    def build_filter_field(self, filt):
        """
        Создает поле фильтра
        :param filt:
        :return:
        """
        choices = self.build_filter_choices(filt)

        validators = []
        if filt.regexp:
            validators.append(RegexValidator(filt.regexp))

        field_kwargs = {
            'required': False,
            'validators': validators,
            'choices': choices,
            'label': _(filt.name),
        }

        field = forms.ChoiceField(**field_kwargs)
        return 'ssdmodification', field

    @staticmethod
    def build_category_field():
        """
        Создает скрытое поле текущей категории
        :return:
        """
        field = forms.CharField(
            required=True,
            widget=forms.HiddenInput(),
        )
        return field

    def build_instance_fields(self, instance):
        """
        Создает поля формы фильтров
        :param instance:
        :return:
        """
        fields = OrderedDict(
            list(map(self.build_filter_field, instance.items))

        )
        fields['category_id'] = self.build_category_field()
        return fields


class FrameSearchFieldsBuilder(FeatureFieldsBuilder):
    """
    Построитель полей для формы поиска по коду кузова
    Реализует создание одного поля для поиска вместо двух раздельных полей,
    принимаемых API Laximo
    """
    def get_frame_full_field_kwargs(self, instance):
        kwargs = super(FeatureFieldsBuilder, self).get_query_arg_field_kwargs(
            'frame_full', instance
        )

        # вставим example в help_text, если он определен
        if hasattr(instance, 'example') and 'help_text' in kwargs:
            kwargs['help_text'] = kwargs['help_text'] % {
                'example': instance.example
            }

        return kwargs

    def build_frame_full_field(self, instance):
        field_kwargs = self.get_frame_full_field_kwargs(instance)
        field = forms.CharField(**field_kwargs)
        return field

    def build_query_args_fields(self, instance=None):
        fields = super(FrameSearchFieldsBuilder, self).build_query_args_fields(
            instance
        )
        fields['frame_full'] = self.build_frame_full_field(instance)
        return fields


class FormMeta(object):
    __slots__ = (
        'name', 'query_cls', 'fields_builder', 'description', 'form_base'
    )

    def __init__(self, name, query_cls, fields_builder, description,
                 form_base=QueryForm):
        self.name = name
        self.query_cls = query_cls
        self.fields_builder = fields_builder
        self.description = description
        self.form_base = form_base


query_cls_meta_map = {
    oem_query.FindVehicleByFrame: FormMeta(
        'framesearch',
        oem_query.FindVehicleByFrame,
        FrameSearchFieldsBuilder,
        _("Search by frame"),
        FrameSearchForm
    ),
    oem_query.FindVehicleByVIN: FormMeta(
        'vinsearch',
        oem_query.FindVehicleByVIN,
        FeatureFieldsBuilder,
        _("Search by VIN"),
    ),
    oem_query.FindVehicleByWizard: FormMeta(
        'wizardsearch2',
        oem_query.GetWizard,
        WizardFieldsBuilder,
        _("Wizard search"),
    ),
    oem_query.ExecCustomOperation: FormMeta(
        'search_vehicle',
        oem_query.ExecCustomOperation,
        OperationFieldsBuilder,
        _("Custom search"),
    ),
}

meta_map = {m.name: m for m in list(query_cls_meta_map.values())}


def build_query_form(meta, instance=None,
                     exclude_query_args=None):
    """
    Создает форму для запроса в сервис Laximo, используя фабрику
    QueryFormFactory

    :param meta: мета-класс формы, содержащий query_cls и fields_builder
    :param instance: экземпляр объекта Laximo, по которому строится форма
    :param exclude_query_args:
    :return:
    """

    if not exclude_query_args:
        exclude_query_args = []

    form_cls = QueryFormFactory(
        meta.query_cls,
        form_cls=meta.form_base,
        fields_builder=meta.fields_builder,
        exclude_query_args=exclude_query_args
    ).create(instance=instance)

    setattr(form_cls, 'Meta', meta)
    setattr(form_cls, 'description', meta.description)

    return form_cls


def get_catalog_forms(catalog, instance_map=None, exclude_forms=None,
                      exclude_query_args=None):
    """
    Создает формы для подбора авто из свойства ```features``` объекта
    CatalogInfo

    :param catalog: экземпляр CatalogInfo
    :param instance_map: mapping всех объектов, полученных в контексте
    текущего запроса в Laximo.
    :param exclude_forms: query_cls, формы которых которые необходимо исключить
    :param exclude_query_args:
    :return:
    """

    if not exclude_forms:
        exclude_forms = []

    if not exclude_query_args:
        exclude_query_args = []

    # todo tests
    def mapper(obj):
        name = getattr(obj, 'kind', None) or getattr(obj, 'name')

        # если meta для такой формы не определен
        if name not in meta_map:
            return None

        form_meta = meta_map[name]
        if form_meta.query_cls in exclude_forms or name in exclude_forms:
            return None

        object_cls = objects.get_query_object(form_meta.query_cls)

        # Если в instance_map не представлен экземпляр object_cls,
        # то фолбэком берем объект, переданный в obj.
        # Это необходимо, например, в случае построения формы по
        # CatalogOperation, где, в отличие от Wizard, инстансом для
        # построения формы является сам obj (представлен в дереве CatalogInfo)
        instance = instance_map.get(object_cls, obj)

        form_cls = build_query_form(
            form_meta, instance=instance,
            exclude_query_args=exclude_query_args
        )

        return form_cls.__name__, form_cls

    catalog_forms = dict((f for f in map(mapper, catalog.features) if f))

    if catalog.extensions:
        catalog_forms.update(
            dict((o for o in map(mapper, catalog.extensions.operations) if o))
        )

    return catalog_forms
