# coding=utf-8
import logging
from collections import namedtuple

from django.core.exceptions import ImproperlyConfigured
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.utils.functional import cached_property
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormMixin

import laximo.oem.objects
from laximo.contrib.django import forms, utils
from laximo.contrib.django.api import (
    get_laximo_objects, request_laximo_api, get_catalog
)
from laximo.contrib.django.decorators import ratelimit
from laximo.contrib.django.exceptions import (
    LaximoHttpResponseFault, BadRequestParamsException,
    UnknownCatalog, NotFoundException, ServiceUnavailableException
)
from laximo.contrib.django.settings import LAXIMO
from laximo.errors import E_NOTSUPPORTED
from laximo.oem import query as oem_query
from laximo.query import QuerySet

logger = logging.getLogger(__name__)


class RatelimitMixin(object):
    ratelimit_limit = LAXIMO.get('RATELIMIT')
    ratelimit_only_unauthenticated = LAXIMO.get('RATELIMIT_ONLY_UNAUTHENTICATED')
    ratelimit_redirect_url = LAXIMO.get('RATELIMIT_REDIRECT_URL')

    def get_ratelimit_config(self):
        return dict(
            limit=self.ratelimit_limit,
            only_unauthenticated=self.ratelimit_only_unauthenticated,
            redirect_url=self.ratelimit_redirect_url,
        )

    def dispatch(self, *args, **kwargs):
        return ratelimit(**self.get_ratelimit_config())(super(RatelimitMixin, self).dispatch)(*args, **kwargs)


class LaximoViewMixin(RatelimitMixin):
    """
    Mixin для представлений, добавляющий общую логику отправки запроса
    в сервис Laximo и обработки ответа от него

    Схема обработки запроса:
    * dispatch()
    * get_context_data()
    * get_object_map()
    * get_queryset()
    * request_laximo_api(queryset)

    Результатм выполнения метода request_laximo_api является объект ответа от
    API сервиса.

    В случае, если ответ содержит данные об ошибке (Fault),
    будет сгенерировано исключение (с объектом Fault kw-аргументе), а View
    вернет ошибку 400.

    В случае корректного ответа от API сервиса, в контексте View
    появятся переменные ```object_map``` и ```object```, где:

        object_map
            словарь со всеми объектами, пришедшими в ответ от API сервиса
            и распознанными библиотекой работы с этим API

        object
            объект из словаря object_map, имеющий являющийся основным для
            данного запроса (см. свойство класса ```object_cls```)
    """
    object_cls = None
    ratelimit_limit = 5
    ratelimit_only_unauthenticated = True
    ratelimit_redirect_url = reverse_lazy('sign')

    @property
    def current_ssd(self):
        """
        Текущий ssd для страницы
        Если передан ssdmodification, то он добавляется к текущему ssd
        :return:
        """
        request = getattr(self, 'request')
        ssd = request.GET.get('ssd', '')
        ssd_modification = request.GET.get('ssdmodification', '')
        return ssd + ssd_modification

    @cached_property
    def current_catalog(self):
        """
        Возвращает текущий каталог
        :return:
        """
        kwargs = getattr(self, 'kwargs')
        code = self.requested_catalog_code or kwargs.get('brand')
        return get_catalog(code)

    @property
    def requested_catalog_code(self):
        """
        Возвращает переданный через querystring код каталога либо None
        :return:
        """
        request = getattr(self, 'request')
        return request.GET.get('catalog')

    @property
    def current_catalog_code(self):
        """
        Возвращает текущий код каталога для страницы
        :return:
        """
        if self.requested_catalog_code:
            return self.requested_catalog_code

        catalog = self.current_catalog
        return catalog.code if catalog else None

    def get_object_cls(self):
        if not self.object_cls:
            raise ImproperlyConfigured(
                "LaximoViewMixin requires either a definition of 'object_cls' "
                "or an implementation of 'get_object_cls()'"
            )
        return self.object_cls

    def get_laximo_queryset(self, queryset=None):
        """
        Набор запросов в сервис Laximo
        Для формирования составных запросов в сервис необходимо
        переопределять в классах-наследниках этот метод по аналогии
        с generic-views' ```get_queryset()```
        :return:
        """
        if queryset:
            return queryset

        return QuerySet()

    def get_object_map(self, queryset=None):
        """
        Возвращает словарь объектов (см. ```laximo.objects```), которые пришли
        в ответ от сервиса Laximo.
        Ключ - класс объекта, значение - его экземпляр
        """
        if not queryset:
            queryset = self.get_laximo_queryset()

        if queryset:
            laximo_response = request_laximo_api(queryset)
            objects_dict = get_laximo_objects(laximo_response)
        else:
            objects_dict = {}

        return objects_dict

    @cached_property
    def object_map(self):
        """
        См. ```get_object_map()```
        :return:
        """
        return self.get_object_map()

    @cached_property
    def object(self):
        """
        Целевой объект запроса (имеющий класс ```object_cls```)
        """
        if not self.object_map:
            return None

        object_cls = self.get_object_cls()

        if object_cls not in self.object_map:
            return None

        return self.object_map[object_cls]

    def get_context_data(self, **kwargs):
        ctx = super(LaximoViewMixin, self).get_context_data(**kwargs)

        # словарь ```object_map```, который идентичен
        # ```self.object_map``` за исключением того, что ключами являются не
        # сами классы объектов, а их имена (для удобного доступа к ним
        # из шаблона)
        ctx['object_map'] = \
            {k.__name__: v for k, v in list(self.object_map.items())}
        ctx['object'] = self.object
        ctx['current_ssd'] = self.current_ssd
        return ctx

    def dispatch(self, request, *args, **kwargs):
        """
        Обрабатывает запрос
        Если в процессе общения с API Laximo брошено исключение
        LaximoHttpResponseFault, формирует ответ HttpResponseBadRequest
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            return super(LaximoViewMixin, self).dispatch(
                request, *args, **kwargs
            )
        except LaximoHttpResponseFault as e:
            logger.warning(
                "Fault request %s, laximo fault: %s" % (
                    request, e.fault.message
                )
            )

            return self.on_api_response_fault(request, e.fault)
        except BadRequestParamsException as e:
            reason = getattr(e, 'message', None)
            logger.warning(
                "Bad request %s, reason: %s" % (request, reason)
            )
            return render(request, 'laximo/error_400.html', status=400)
        except NotFoundException as e:
            logger.warning(
                "Not found with request %s, queryset: %s" % (
                    request, e.queryset
                )
            )
            return render(request, 'laximo/error_404.html', status=404)
        except ServiceUnavailableException:
            logger.warning(
                "Laximo ServiceUnavailable, request %s" % (
                    request
                )
            )
            return render(request, 'laximo/error_unavailable.html', status=503)

    def on_api_response_fault(self, request, fault):
        return render(request, 'laximo/error_500.html', status=500)


class PrefetchCatalogMixin(LaximoViewMixin):
    """
    Выполняет prefetch данных по запросу GetCatalogInfo

    Метод ```get_catalog_object()``` будет возвращать объект не обращаясь
    к ```object_map```, тем самым не инициируя раньше времени составной запрос.

    Позволяет, например, построить формы из extensions для того, чтобы, после
    их валидации, отправить данные следующим запросом
    """

    @cached_property
    def prefetched(self):
        catalog_code = self.current_catalog_code

        if not catalog_code:
            return {}

        query = oem_query.GetCatalogInfo(
            catalog=catalog_code,
            ssd=self.current_ssd,
        )

        return self.get_object_map(QuerySet(query))

    def get_catalog_object(self):
        object_map = self.prefetched
        return object_map.get(laximo.oem.objects.CatalogInfo)


class CatalogMixin(LaximoViewMixin):
    """
    Примесь, позволяющая определять запрошен ли Wizard для показа на странице
    """

    @property
    def show_wizard(self):
        kwargs = getattr(self, 'kwargs')
        brand = kwargs.get('brand')
        catalog = get_catalog(brand)
        return catalog.is_wizard_available if catalog else False


class WizardMixin(CatalogMixin):
    """
    Добавляет поддержку формы поиска авто через Wizard
    """

    def get_laximo_queryset(self, queryset=None):
        """
        Добавляет запрос GetWizard в Laximo QuerySet
        :param queryset:
        :return:
        """
        if not queryset:
            queryset = super(WizardMixin, self).get_laximo_queryset(queryset)

        if self.show_wizard:
            queryset.add_query(
                oem_query.GetWizard(
                    catalog=self.current_catalog_code,
                    ssd=self.current_ssd,
                )
            )

        return queryset


class CatalogInfoMixin(CatalogMixin):
    """
    Добавляет поддержку получения данных каталога и отображения
    форм для подбора авто, исходя из пришедних в ответ от сервиса
    свойства ```features``` у объекта каталога
    """

    def get_catalog_object(self):
        """
        Возвращает экземпляр CatalogInfo
        :return:
        """
        return self.object_map.get(laximo.oem.objects.CatalogInfo)

    def get_exclude_forms(self):
        """
        Возвращает формы, которые необходимо исключить при создании набора из
        объекта CatalogInfo
        :return:
        """
        exclude_list = []

        if not self.show_wizard:
            exclude_list.append(oem_query.GetWizard)

        return exclude_list

    def get_features_forms(self):
        """
        Возвращает формы, исходя из свойства ```features``` у объекта каталога
        :return: dict
        """
        catalog_object = self.get_catalog_object()
        exclude_forms = self.get_exclude_forms()

        if not catalog_object:
            return {}

        form_classes = forms.get_catalog_forms(
            catalog_object, self.object_map, exclude_forms=exclude_forms,
            exclude_query_args=['catalog'],
        )

        return form_classes

    def get_laximo_queryset(self, queryset=None):
        """
        Добавляет запрос GetCatalogInfo в Laximo QuerySet
        :param queryset:
        :return:
        """
        catalog_code = self.current_catalog_code

        queryset = super(CatalogInfoMixin, self).get_laximo_queryset()

        if catalog_code:
            queryset.add_query(
                oem_query.GetCatalogInfo(
                    catalog=catalog_code,
                    ssd=self.current_ssd,
                )
            )

        return queryset

    def get_context_data(self, **kwargs):
        """
        Добавляет формы для подбора авто в контекст
        :param kwargs:
        :return:
        """
        ctx = super(CatalogInfoMixin, self).get_context_data(**kwargs)
        features_forms = self.get_features_forms()
        ctx['form'] = features_forms
        return ctx


class VehicleInfoMixin(LaximoViewMixin):
    def get_object_map(self, queryset=None):
        object_map = super(VehicleInfoMixin, self).get_object_map(queryset)
        vehicle = object_map.get(laximo.oem.objects.Vehicle)
        if not vehicle:
            raise NotFoundException(queryset=queryset)
        return object_map

    def get_laximo_queryset(self, queryset=None):
        kwargs = getattr(self, 'kwargs')

        queryset = super(VehicleInfoMixin, self).get_laximo_queryset()
        queryset.add_query(
            oem_query.GetVehicleInfo(
                catalog=self.current_catalog_code,
                vehicle_id=kwargs.get('vehicle_id'),
                ssd=self.current_ssd,
            )
        )

        return queryset


class CategoryMixin(object):
    """
    Примесь, позволяющая получать текущую запрошенную категорию
    """

    @property
    def current_category(self):
        """
        Выбранная категория
        :return:
        """
        request = getattr(self, 'request')
        kwargs = getattr(self, 'kwargs')

        val = kwargs.get('category_id') or request.GET.get('category_id') or -1
        return val

    def get_context_data(self, **kwargs):
        """
        Добавляет текущую категорию в контекст
        :param kwargs:
        :return:
        """
        ctx = super(CategoryMixin, self).get_context_data(**kwargs)
        ctx['current_category'] = self.current_category
        return ctx


class CategoryListMixin(LaximoViewMixin, CategoryMixin):
    def get_category_list_object(self):
        """
        Возвращает объект CategoryList
        :return:
        """
        return self.object_map[laximo.oem.objects.CategoryList]

    def get_laximo_queryset(self, queryset=None):
        kwargs = getattr(self, 'kwargs')

        queryset = super(CategoryListMixin, self).get_laximo_queryset()
        queryset.add_query(
            oem_query.ListCategories(
                catalog=self.current_catalog_code,
                vehicle_id=kwargs.get('vehicle_id'),
                category_id=self.current_category,
                ssd=self.current_ssd,
            )
        )

        return queryset


class UnitListMixin(LaximoViewMixin, CategoryMixin):
    """
    Отображает список узлов автомобиля
    """

    def get_unit_list_object(self):
        return self.object_map[laximo.oem.objects.UnitList]

    def get_laximo_queryset(self, queryset=None):
        kwargs = getattr(self, 'kwargs')

        queryset = super(UnitListMixin, self).get_laximo_queryset()
        queryset.add_query(
            oem_query.ListUnits(
                catalog=self.current_catalog_code,
                vehicle_id=kwargs.get('vehicle_id'),
                category_id=self.current_category,
                ssd=self.current_ssd,
            )
        )

        return queryset


class CatalogListView(LaximoViewMixin, TemplateView):
    """
    Отображает список каталогов
    """
    object_cls = laximo.oem.objects.CatalogList
    template_name = 'laximo/catalog_list.html'

    def get_laximo_queryset(self, queryset=None):
        return QuerySet(
            oem_query.ListCatalogs()
        )


class CatalogInfoView(CatalogInfoMixin, WizardMixin, TemplateView):
    """
    Отображает информацию каталога с формами для поиска авто
    """
    object_cls = laximo.oem.objects.CatalogInfo
    template_name = 'laximo/catalog_info.html'

    def get_catalog_object(self):
        """
        Если не удается получить объект каталога стандартным образом,
        бросается исключение ```UnknownCatalog```

        ```UnknownCatalogMiddleware```, в свою очередь, пытается получить
        каталог по переданному бренду и, если это удается, редиректит
        на соответствующую страницу с параметром кода каталога.
        :return:
        """
        catalog = super(CatalogInfoView, self).get_catalog_object()
        if not catalog:
            raise UnknownCatalog(
                "Can not resolve catalog for brand %s" %
                self.kwargs.get('brand')
            )
        return catalog


class WizardView(CatalogInfoMixin, WizardMixin, FormMixin, TemplateView):
    """
    Обрабатывает поиск авто через Wizard
    """
    object_cls = laximo.oem.objects.Wizard
    template_name = 'laximo/wizard.html'
    wizard_requested = True

    def get_form_class(self):
        features_forms = self.get_features_forms()
        return features_forms['GetWizardForm']

    def get_laximo_queryset(self, queryset=None):
        qs = super(WizardView, self).get_laximo_queryset()
        return qs

    def get_context_data(self, **kwargs):
        ctx = super(WizardView, self).get_context_data(**kwargs)
        ctx['form'] = self.get_features_forms()
        return ctx


class VehicleListView(PrefetchCatalogMixin, CatalogInfoMixin,
                      WizardMixin, FormMixin, TemplateView):
    """
    Отображает список найденных автомобилей

    Поддерживает несколько видов поиска: через Wizard, по VIN,
    по номеру кузова, через ExecCustomOperation

    Имя формы, через которую производится поиск, передается через
    GET-параметр ```f```. Для переданного значения ищется подходящая связка
    для построения формы в ```form_map```.

    Если связки нет, бросается исключение ```BadRequestParamsException```

    По найденной связке строится соответствующая запросу форма, которая,
    после прохождения, валидации, модифицирует QuerySet для запроса в Laximo

    Именованный кортеж Bundle содержит три поля:
        query_cls:
            класс запроса в Laximo для построения формы
        object_cls:
            модель объекта ответа от сервиса Laximo, возвращающийся
            при выполенении запроса
        get_instance:
            функция, принимающая два аргумента: текущий каталог и ```request```
            должна возвращать экземпляр объекта, необходимый для построения
            формы

    Полученные от сервиса данные в соответствующем объекте
    (см. ```Bundle.object_cls```) доступны в контексте ответа, для итерации
    по ним используется свойство items
    """
    template_name = 'laximo/vehicle_list.html'
    object_cls = laximo.oem.objects.FindVehicleByWizard

    Bundle = namedtuple(
        'Bundle', ['query_cls', 'object_cls', 'get_instance'])

    # todo: эту чехарду можно точно как-то упростить
    # 1. классы oem.* знают об объектах Laximo через meta
    # 2. см. forms.meta_map - примерно та же самая конфигурация
    #
    # В целом эти навороты нужны, т.к.forms.meta_map используется методом
    # forms.get_catalog_forms, возвращающим все формы, поддерживаемые данным
    # каталогом, но там используются данные из ответа от API laximo.
    #
    # Но когда нам необходимо построить форму ДО вызова API, мы еще
    # ничего не знаем о том, какие объекты к нам вернутся. Объект
    # CatalogInfo запрашивается через prefetch (см. PrefetchCatalogMixin)
    # и конкретную форму, по которой мы отображаем список найденных авто,
    # мы можем построить и отвалидировать ДО отправки данных полным запросом в
    # API Laximo

    form_map = {
        'getwizardform': Bundle(
            oem_query.FindVehicleByWizard,
            laximo.oem.objects.FindVehicleByWizard, None
        ),
        'findvehiclebyvinform': Bundle(
            oem_query.FindVehicleByVIN, laximo.oem.objects.FindVehicleByVIN,
            lambda catalog, request: utils.get_catalog_feature(
                catalog, 'vinsearch'
            )
        ),
        'findvehiclebyframeform': Bundle(
            oem_query.FindVehicleByFrame,
            laximo.oem.objects.FindVehicleByFrame,
            lambda catalog, request: utils.get_catalog_feature(
                catalog, 'framesearch'
            )
        ),
        'execcustomoperationform': Bundle(
            oem_query.ExecCustomOperation,
            laximo.oem.objects.ExecCustomOperation,
            utils.get_catalog_operation
        )
    }

    def get_object_cls(self):
        """
        Возваращет ожидаемый класс объекта из ответа от Laximo
        :return:
        """
        form_name = self.get_form_name()
        return self.form_map.get(form_name).object_cls

    def get_query_cls(self):
        """
        Возвращает класс запроса в Laximo
        :return:
        """
        form_name = self.get_form_name()
        return self.form_map.get(form_name).query_cls

    def get_instance(self):
        """
        Возвращает экземпляр из ответа от Laximo, необходимый
        для построения формы
        :return:
        """
        form_name = self.get_form_name()
        bundle = self.form_map.get(form_name)
        catalog_object = self.get_catalog_object()

        if catalog_object and bundle.get_instance:
            return bundle.get_instance(
                catalog_object, self.request
            )

        return None

    def get_laximo_queryset(self, queryset=None):
        qs = super(VehicleListView, self).get_laximo_queryset(queryset)

        try:
            query_cls = self.get_query_cls()
            query_kwargs = self.get_query_kwargs()
            query = query_cls(**query_kwargs)
            qs.add_query(query)
        except BadRequestParamsException:
            pass

        return qs

    def get_query_kwargs(self):
        """
        Возвращает kw-аргументы, передаваемый в соответствующий
        ```laximo.query```
        :return:
        """
        query_kwargs = {
            'catalog': self.request.GET.get('catalog', ''),
            'ssd': self.request.GET.get('ssd', ''),
        }

        form = self.get_form()

        if form.is_valid():
            query_kwargs.update(form.cleaned_data)
        else:
            raise BadRequestParamsException(form=form)

        return query_kwargs

    def get_form_name(self):
        """
        Возвращает имя формы, через которую производится поиск
        :return:
        """
        form_name = self.request.GET.get('f')
        if not form_name:
            raise BadRequestParamsException("No form name passed")
        return form_name.lower()

    def get_form_class(self):
        """
        Возвращает класс формы, через которую производится поиск
        :return:
        """
        query_cls = self.get_query_cls()
        form_meta = forms.query_cls_meta_map[query_cls]

        instance = self.get_instance()

        form_cls = forms.build_query_form(
            form_meta, instance, exclude_query_args=['catalog']
        )

        return form_cls

    def get_form_kwargs(self):
        """
        Возвращает данные для инициализации формы, через которую производится
        поиск
        :return:
        """
        kwargs = super(VehicleListView, self).get_form_kwargs()
        kwargs.update({
            'data': self.request.GET,
        })
        return kwargs

    def get_exclude_forms(self):
        """
        Возвращает формы, которые необходимо исключить при создании набора
        форм через фабрику
        :return:
        """
        form = self.get_form()

        exclude_list = super(VehicleListView, self).get_exclude_forms()
        exclude_list += [form.Meta.query_cls]

        return exclude_list

    def get_features_forms(self):
        """
        Переопределяет поведение ```get_features_forms```, добавляя в
        результирующий словарь текущую форму, через которую производится
        поиск (она же должна быть исключена из построения через фабрику
        в методе ```get_exclude_forms()```)
        :return:
        """
        form = self.get_form()
        form_classes = super(VehicleListView, self).get_features_forms()
        form_classes[form.__class__.__name__] = form

        return form_classes

    def get_context_data(self, **kwargs):
        ctx = super(VehicleListView, self).get_context_data(**kwargs)
        object_map = ctx.get('object_map', {})

        if not object_map.get(laximo.oem.objects.CatalogInfo.__name__, None):
            # Если в контексте в object_map отсутствует текущий каталог,
            # Попытаемся получить его исходя из списка найденных авто
            catalog = utils.get_vehicle_catalog(self.object_map)
            object_map[laximo.oem.objects.CatalogInfo.__name__] = catalog
            ctx['object_map'] = object_map

        return ctx

    def get_object_map(self, queryset=None):
        """
        Возвращает маппинг объектов, пришедних от сервиса, обрабатывая некоторые исключительные ситуации
        :param queryset:
        :return:
        """
        try:
            object_map = super(VehicleListView, self).get_object_map(queryset=queryset)
        except LaximoHttpResponseFault as e:
            # если ввести неверный VIN, в отличие от неверного номера кузова,
            # в ответ вернутся Fault, а не пустой список, поэтому обработаем
            # этот случай отдельно
            object_cls = self.get_object_cls()
            if e.fault.fault_info[1] == 'VIN':
                # возвращаем пустой объект
                return {
                    object_cls: object_cls()
                }
            raise e

        return object_map


class VehicleQuickGroupsView(CatalogInfoMixin, VehicleInfoMixin, TemplateView):
    """
    Отображает список быстрых групп
    """
    template_name = 'laximo/vehicle_quickgroups.html'
    object_cls = laximo.oem.objects.QuickGroupTree

    def get_laximo_queryset(self, queryset=None):
        qs = super(VehicleQuickGroupsView, self).get_laximo_queryset(queryset)

        qs.add_query(
            oem_query.ListQuickGroup(
                catalog=self.current_catalog_code,
                vehicle_id=self.kwargs.get('vehicle_id'),
                ssd=self.current_ssd,
            )
        )

        return qs

    def on_api_response_fault(self, request, fault):
        """
        Когда при выборе авто недоступен список деталей
        вместо ошибки LaximoHttpResponseFault мы должны попытаться
        перенаправить пользователя на поиск по изображениям

        :param request:
        :param fault:
        :return:
        """
        if fault.code == E_NOTSUPPORTED:
            brand = request.resolver_match.kwargs.get('brand')
            vehicle_id = request.resolver_match.kwargs.get('vehicle_id')
            url = "%s?%s" % (
                reverse('laximo:vehicle_unit_list', args=(brand, vehicle_id)),
                request.GET.urlencode()
            )
            return redirect(url)
        else:
            return super(VehicleQuickGroupsView, self).on_api_response_fault(
                request, fault
            )


class VehicleQuickDetailsView(CatalogInfoMixin, VehicleInfoMixin,
                              TemplateView):
    """
    Отображает список деталей
    """
    template_name = 'laximo/vehicle_quickdetails.html'
    object_cls = laximo.oem.objects.QuickDetailTree

    def get_laximo_queryset(self, queryset=None):
        qs = super(VehicleQuickDetailsView, self).get_laximo_queryset(queryset)
        ssd = self.request.GET.get('ssd', '')

        qs.add_query(
            oem_query.ListQuickDetail(
                catalog=self.current_catalog_code,
                quick_group_id=self.kwargs.get('group_id'),
                vehicle_id=self.kwargs.get('vehicle_id'),
                ssd=ssd,
            )
        )

        return qs


class VehicleUnitListView(CatalogInfoMixin, VehicleInfoMixin, UnitListMixin,
                          CategoryListMixin, TemplateView):
    """
    Отображает список узлов
    """
    template_name = 'laximo/vehicle_unit_list.html'
    object_cls = laximo.oem.objects.UnitList


class UnitInfoMixin(LaximoViewMixin):
    """
    Примесь для получения информациии об узле автомобиля
    """

    def get_laximo_queryset(self, queryset=None):
        queryset = super(UnitInfoMixin, self).get_laximo_queryset(
            queryset=queryset
        )

        kwargs = getattr(self, 'kwargs')

        queryset.add_query(
            oem_query.GetUnitInfo(
                catalog=self.current_catalog_code,
                unit_id=kwargs.get('unit_id'),
                ssd=self.current_ssd,
            )
        )

        return queryset


class VehicleUnitFilterView(CatalogInfoMixin, VehicleInfoMixin, UnitInfoMixin,
                            CategoryMixin, FormMixin, TemplateView):
    """
    Дополнительная фильтрация автомобиля, если требуется уточнение на этапе
    выбора деталей узла
    """
    template_name = 'laximo/vehicle_unit_filter.html'
    object_cls = laximo.oem.objects.UnitFilters

    def get_laximo_queryset(self, queryset=None):
        queryset = super(VehicleUnitFilterView, self).get_laximo_queryset(
            queryset=queryset
        )

        kwargs = getattr(self, 'kwargs')

        queryset.add_query(
            oem_query.GetFilterByUnit(
                catalog=self.current_catalog_code,
                unit_id=kwargs.get('unit_id'),
                vehicle_id=kwargs.get('vehicle_id'),
                filter=kwargs.get('filter')
            )
        )

        return queryset

    def get_form_class(self):
        form_cls = forms.QueryFormFactory(
            oem_query.GetFilterByUnit,
            fields_builder=forms.FilterFieldsBuilder,
            exclude_query_args=('catalog', 'unit_id', 'vehicle_id', 'filter')
        ).create(self.object)
        return form_cls

    def get_form_kwargs(self):
        kwargs = super(VehicleUnitFilterView, self).get_form_kwargs()
        kwargs.update({
            'initial': {
                'ssd': self.current_ssd,
                'category_id': self.current_category,
            }
        })
        return kwargs

    def get_context_data(self, **kwargs):
        ctx = super(VehicleUnitFilterView, self).get_context_data(**kwargs)
        ctx['form'] = self.get_form()
        return ctx


class VehicleUnitDetails(CatalogInfoMixin, VehicleInfoMixin, UnitInfoMixin,
                         CategoryMixin, TemplateView):
    """
    Список деталей узла
    """
    template_name = 'laximo/vehicle_unit_details.html'
    object_cls = laximo.oem.objects.Unit

    def get_laximo_queryset(self, queryset=None):
        queryset = super(VehicleUnitDetails, self).get_laximo_queryset(
            queryset
        )

        queryset.add_query(
            oem_query.ListDetailByUnit(
                catalog=self.current_catalog_code,
                unit_id=self.kwargs.get('unit_id'),
                ssd=self.current_ssd,
            )
        )

        queryset.add_query(
            oem_query.ListImageMapByUnit(
                catalog=self.current_catalog_code,
                unit_id=self.kwargs.get('unit_id'),
                ssd=self.current_ssd,
            )
        )

        return queryset

    def get_context_data(self, **kwargs):
        ctx = super(VehicleUnitDetails, self).get_context_data(**kwargs)
        ctx['highlighted'] = self.request.GET.get('highlighted', '').split(',')
        return ctx
