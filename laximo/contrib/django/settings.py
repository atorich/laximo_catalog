from django.conf import settings as settings
from django.core.exceptions import ImproperlyConfigured

from laximo import settings as lib_settings

LAXIMO = getattr(settings, 'LAXIMO', {})

LAXIMO.setdefault('CACHE_TIMEOUT', 0)
LAXIMO.setdefault('CACHE_KEY_PREFIX', 'laximo_page')
LAXIMO.setdefault('CACHE_ALIAS', None)

LAXIMO.setdefault('RATELIMIT', 5)  # per day
LAXIMO.setdefault('RATELIMIT_TIMEOUT', 86400)  # per day
LAXIMO.setdefault('RATELIMIT_CACHE_BACKED', 'default')
LAXIMO.setdefault('RATELIMIT_ONLY_UNAUTHENTICATED', True)  # by default
LAXIMO.setdefault('RATELIMIT_REDIRECT_URL', 'sign')


if LAXIMO.get('RATELIMIT') and LAXIMO.get('RATELIMIT_CACHE_BACKED') not in settings.CACHES:
    raise ImproperlyConfigured("You should specify valid cache backend when RATELIMIT is enabled")


def setup_laximo_lib():
    for key, value in list(LAXIMO.items()):
        if hasattr(lib_settings, key):
            setattr(lib_settings, key, value)
