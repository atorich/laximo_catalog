(function ($, window) {
    $.fn.tree = function (options) {
        var $tree = $(this),
            index = {},
            settings = $.extend({
                toggleClass: '.tree-toggle',
                commonNodeClass: 'fa',
                openedNodeClass: 'fa-minus-square',
                closedNodeClass: 'fa-plus-square',
                filterKeySelector: '.name:first',
                filterInputSelector: '.tree-filter',
                filterResultSelector: '.filter-results',
                filterNoResultText: 'Ничего не найдено'
            }, options);

        var bindToggle = function () {
            $(settings.toggleClass, $tree).click(function () {
                var $list = $(this).parent().children('ul');

                $list.toggleClass('expanded');

                $(this).toggleClass(settings.closedNodeClass);
                $(this).toggleClass(settings.openedNodeClass);
            });
        };

        /**
         * Устанавливает класс корневому элементу
         */
        var initRoot = function () {
            $tree.find('.root').addClass('expanded');
        };

        /**
         * Устанавливает классы элементам списка (кроме листьев)
         */
        var initToggle = function () {
            $tree.find('li:not(".leaf") > ' + settings.toggleClass)
                .addClass(settings.commonNodeClass)
                .addClass(settings.closedNodeClass);

        };

        /**
         * Инициализирует фильтр
         */
        var initFilter = function () {
            indexTree();

            $(settings.filterInputSelector).on('keyup', function (e) {
                var $this = $(e.target);

                clearFilterResults();

                if ($this.val().length <= 3) {
                    $tree.show();
                    return false;
                }

                $tree.hide();
                findNode($this.val(), nodeMatchCallback);
            });
        };

        var findNode = function (text, callback) {
            var found = 0;
            text = text.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

            $.each(index, function (key, node) {
                if (key.match(text.toLowerCase())) {
                    callback(node);
                    found++;
                }
            });

            if (!found) {
                onFilterNoResult();
            }
        };

        var clearFilterResults = function () {
            $(settings.filterResultSelector).html("");
        };

        var onFilterNoResult = function () {
            $(settings.filterResultSelector).html(settings.filterNoResultText);
        };

        var nodeMatchCallback = function ($node) {
            $(settings.filterResultSelector).append($node);
        };

        /**
         * Индексирует дерево для корректной работы фильтра
         */
        var indexTree = function () {
            $.each($tree.find('li'), function () {
                var key, $this = $(this);
                key = $this.find(settings.filterKeySelector)
                    .text().replace(/\s+|\n|\/|\r\n|,|/g, '').toLowerCase();
                index[key] = $this;
            });
        };

        var isFilterEnabled = function () {
            return settings.filterInputSelector
                && settings.filterResultSelector;
        };

        var init = function () {
            initRoot();
            initToggle();
            bindToggle();

            if (isFilterEnabled()) {
                initFilter();
            }
        };

        init();

        return $tree;
    };
})($, window);