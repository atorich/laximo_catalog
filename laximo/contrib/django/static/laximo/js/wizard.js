(function (window, $) {
    /**
     * Логика работы формы Wizard
     */

    $.fn.wizardForm = function () {
        var $form = $(this);

        var onFormChange = function (e) {
            var $form, $targetField;

            $form = $(e.currentTarget);
            $targetField = $(e.target);

            $form.find('#id_ssd').val($targetField.val());
            $form.trigger('submit');
        };

        var processDetermined = function (_, elem) {
            elem.options[elem.options.length - 1].selected = true;
        };

        $form.change(onFormChange);
        $.each($('[data-determined]'), processDetermined);
    };
})(window, jQuery);
