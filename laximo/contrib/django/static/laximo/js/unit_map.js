(function (window, $) {
    if (!$.fn.mapster) {
        throw "Mapster not found. Please, install it: https://github.com/jamietre/ImageMapster"
    }

    $.fn.initUnitMap = function (options) {
        /**
         * Связывает изображение, маппинг с областями и список
         * элементов
         *
         * При наведении мышкой на элемент или область включается подсветка
         * соответствующей пары посредством добавления элементу в списке
         * класса, переданного настройках через атрибут itemHighlightedClass
         *
         * При клике на элемент происходит выбор пары посредством добавления
         * элементу в списке класса, переданного в настройках через атрибут
         * itemSelectedClass
         *
         * Изменение размера изображения доступно при прокрутке колеса мышки
         * над изображением (см. ```initScaler```)
         *
         * При измененном размере изображения возможна drag/drop прокрутка
         * для отображения скрытой части (см. ```initDragScroll```)
         *
         * Пример использования:
         *
         *  <div id="example-map-container">
         *      <img id="example-img" usemap="#example-map" src="..."/>
         *      <ul>
         *          <li id="item1">Item1</li>
         *          <li id="item2">Item2</li>
         *      </ul>
         *
         *      <map name="example-map" id="example-map">
         *          <area id="area1" data-code="1" shape="rect" coords="1 2 1 2">
         *          <area id="area2" data-code="1" shape="rect" coords="4 6 4 6">
         *      </map>
         *   </div>
         *
         *   $('#example-map-container').initUnitMap({
         *       imageSelector: '#example-img',
         *       itemIdStartsWith: 'item',
         *       areaIdStartsWith: 'area',
         *       imageWrapperWidth: 600,
         *       imageWrapperHeight: 600
         *   });
         *
         *
         * Доступные настройки:
         *  imageSelector
         *      селектор, по которому доступно исходное изображение
         *      по умолчанию ```#unit-img```
         *
         *  itemIdStartsWith
         *      текст, с которого начинается ID элемента списка
         *      по умолчанию ```item```
         *
         *  areaIdStartsWith
         *      текст, с которого начинается ID <area>
         *      по умолчанию ```area```
         *
         *  areaCodeKey
         *      data-атрибут, содержащий в элемента <area> код детали
         *      по умолчанию ```data-code```
         *
         *  itemClass
         *      класс элемента списка
         *      по умолчанию ```item```
         *
         *  itemHighlightedClass
         *      класс, добавляемый для подсветки
         *      по умолчанию ```highlighted```
         *
         *  itemSelectedClass
         *      класс, добавляемый для отображения выбора
         *      по умолчанию ```selected```
         *
         *   imageWrapperClass
         *      класс, присваиваемый обертке для исходного изображения
         *      по умолчанию ```unit-img-wrapper```
         *
         *   imageWrapperWidth
         *      ширина контейнера, задаваемая при инициализации
         *      если не передана, то используется вычисляемое значение
         *      по умолчанию null
         *
         *   imageWrapperHeight:
         *      высота контейнера, задаваемая при инициализации
         *      если не передана, то используется вычисляемое значение
         *      по умолчанию null
         *
         *   imageResizeStep
         *      шаг при ресайзе изображения
         *      по умолчанию 0.1
         *
         *   mapsterOptions
         *      настройки ImageMapster
         *      см: https://github.com/jamietre/ImageMapster
         */
        var $imgWrapper, $img, $container;

        var opts, settings = $.extend({
            imageSelector: '#unit-img',
            itemIdStartsWith: 'item',
            areaIdStartsWith: 'area',

            areaCodeKey: 'data-code',
            itemClass: 'item',
            itemHighlightedClass: 'highlighted',
            itemSelectedClass: 'selected',

            imageWrapperClass: 'unit-img-wrapper',
            imageWrapperWidth: null,
            imageWrapperHeight: null,
            imageResizeStep: 0.1,
            mapsterOptions: {
                render_highlight: {
                    fill: true,
                    fillColor: 'FFFFFF',
                    fillColorMask: '000000',
                    fillOpacity: 0.75,
                    stroke: true,
                    strokeColor: 'F98803',
                    strokeOpacity: 1,
                    strokeWidth: 2
                },
                render_select: {
                    fill: true,
                    fillColor: 'FFFFFF',
                    fillColorMask: '000000',
                    fillOpacity: 0.75,
                    stroke: true,
                    strokeColor: 'c8081e',
                    strokeOpacity: 1,
                    strokeWidth: 2
                }
            }
        }, options);

        $container = $(this);
        $img = $(settings.imageSelector);

        /**
         * Создает обертку для изображения
         */
        var wrapImage = function () {
            var $_imgWrapper, wWidth, wHeight;

            $_imgWrapper = $('<div/>');

            $_imgWrapper.addClass(settings.imageWrapperClass);

            $img.wrap($_imgWrapper);
            $imgWrapper = $('.' + settings.imageWrapperClass);

            wWidth = settings.imageWrapperWidth || $imgWrapper.width();
            wHeight = settings.imageWrapperHeight || $imgWrapper.height();

            $imgWrapper.css({
                minWidth: wWidth,
                minHeight: wHeight,
                maxWidth: wWidth,
                maxHeight: wHeight,
                overflow: 'auto'
            });
        };

        /**
         * Возвращает ID элемента с кодом code
         * @param code
         * @returns {string}
         */
        var getItemId = function (code) {
            return '#' + settings.itemIdStartsWith + code;
        };

        /**
         * Возвращает ID area с кодом code
         * @param code
         * @returns {string}
         */
        var getAreaId = function (code) {
            return '#' + settings.areaIdStartsWith + code;
        };

        /**
         * Обработчик событий мышки для area
         * @param e
         */
        var areaMouseEventHandler = function (e) {
            var domEvent, code, $item;

            domEvent = e.e;
            code = domEvent.currentTarget.dataset.code;
            $item = $(getItemId(code));

            switch (e.e.type) {
                case 'mouseover':
                    $item.addClass(settings.itemHighlightedClass);
                    break;
                case 'mouseout':
                    $item.removeClass(settings.itemHighlightedClass);
                    break;
                case 'click':
                    if (e.selected) {
                        $item.addClass(settings.itemSelectedClass);
                    } else {
                        $item.removeClass(settings.itemSelectedClass);
                    }
                    break;
                default:
                    throw "Undefined event type";
            }
        };

        /**
         * Обработчик событий мышки для элемента списка
         * @param e
         */
        var itemMouseEventHandler = function (e) {
            var code, $item, $area;

            $item = $(e.currentTarget);
            code = $item.data().code;
            $area = $(getAreaId(code));

            switch (e.type) {
                case 'mouseover':
                    $item.addClass(settings.itemHighlightedClass);
                    $area.mapster('highlight');
                    break;
                case 'mouseout':
                    $item.removeClass(settings.itemHighlightedClass);
                    $img.mapster('highlight', false);
                    break;
                case 'click':
                    if ($area.mapster('get')) {
                        $area.mapster('deselect');
                        $item.removeClass(settings.itemSelectedClass);
                    } else {
                        $area.mapster('select');
                        $item.addClass(settings.itemSelectedClass);
                    }
                    break;
                default:
                    throw "Undefined event type";
            }
        };

        var areaStateChangeEventHandler = function (arguments) {
        };

        /**
         * Изменяет размер изображения на ```100-mul``` процентов
         * Если итоговый размер получается меньше изначально, присваиваются
         * значения последнего
         * @param mul
         */
        var resizeImg = function (mul) {
            var width, height, newHeight, newWidth, wrapWidth, wrapHeight;

            width = $img.width();
            height = $img.height();

            newWidth = width * mul;
            newHeight = height * mul;

            wrapWidth = $imgWrapper.width();
            wrapHeight = $imgWrapper.height();

            if (newWidth < wrapWidth &&
                newHeight < wrapHeight) {
                $img.mapster(
                    'resize',
                    $img.data('initialWidth'),
                    $img.data('initialHeight'),
                    75
                );
                return;
            }

            $img.mapster('resize', newWidth, newHeight, 75);
        };

        /**
         * Обрабатывает скролл изображения
         * @param top
         * @param left
         */
        var scrollImg = function (top, left) {
            var currentScrollTop, currentScrollLeft,
                newScrollTop, newScrollLeft;

            currentScrollTop = $imgWrapper.scrollTop();
            currentScrollLeft = $imgWrapper.scrollLeft();

            newScrollTop = currentScrollTop - top;
            newScrollLeft = currentScrollLeft - left;

            $imgWrapper.scrollTop(newScrollTop);
            $imgWrapper.scrollLeft(newScrollLeft);
        };

        /**
         * Обработчик события колеса мышки над изображением
         * @param e
         */
        var imageWheelEventHandler = function (e) {
            if (!$.fn.mousewheel) {
                throw "Mousewheel plugin not found. Please, install it: https://github.com/jquery/jquery-mousewheel"
            }

            if (e.deltaY > 0) {
                resizeImg(1 + settings.imageResizeStep);
            }

            if (e.deltaY < 0) {
                resizeImg(1 - settings.imageResizeStep);
            }

            e.preventDefault(e);
        };

        /**
         * Инициализирует изменение масштаба
         */
        var initScaler = function () {
            $img.data('initialWidth', $img.width());
            $img.data('initialHeight', $img.height());
            $img.on('mousewheel', imageWheelEventHandler);
        };

        /**
         * Инициализирует скролл изображения
         */
        var initDragScroll = function () {
            var mouseDownHandler = function (e) {
                if (!e.data) {
                    e.data = {}
                }

                e.data.lastPosition = {
                    left: e.clientX,
                    top: e.clientY
                };

                $.event.add(document, 'mouseup', mouseUpHandler, e.data);
                $.event.add(document, 'mousemove', mouseMoveHandler, e.data);
                $imgWrapper.addClass('scrolling');
                e.preventDefault();
            };

            var mouseUpHandler = function (e) {
                $.event.remove(document, 'mousemove', mouseMoveHandler);
                $.event.remove(document, 'mouseup', mouseUpHandler);
                $imgWrapper.removeClass('scrolling');
                e.preventDefault();
            };

            var mouseMoveHandler = function (e) {
                var delta = {
                    left: (e.clientX - e.data.lastPosition.left),
                    top: (e.clientY - e.data.lastPosition.top)
                };

                scrollImg(delta.top, delta.left);

                e.data.lastPosition = {
                    left: e.clientX,
                    top: e.clientY
                };

                e.preventDefault();
            };

            $img.on('mousedown', mouseDownHandler);
        };

        /**
         * Инициализирует плагин
         */
        var init = function () {
            wrapImage();
            initScaler();
            initDragScroll();

            $container.on('mouseover', '.item', itemMouseEventHandler);
            $container.on('mouseout', '.item', itemMouseEventHandler);
            $container.on('click', '.item', itemMouseEventHandler);

            opts = $.extend({
                onMouseover: areaMouseEventHandler,
                onMouseout: areaMouseEventHandler,
                onClick: areaMouseEventHandler,
                onStateChange: areaStateChangeEventHandler,
              mapKey: settings.areaCodeKey,
              configTimeout: 29000
            }, settings.mapsterOptions);

            $img.mapster('unbind').mapster(opts);
        };

        init();
    };
})(window, jQuery);