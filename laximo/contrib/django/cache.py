# coding: utf-8


from django.conf import settings
from django.core.cache import caches
from django.views.decorators.cache import cache_page

from laximo.contrib.django import settings as laximo_settings


def cache_catalog_page(view_func):
    """
    Декоратор для кеширования страницы каталога
    Проксирует вызов стандартного cache_page, но параметры берет из настроек
    :param view_func:
    :return:
    """
    cache_alias = laximo_settings.LAXIMO.get('CACHE_ALIAS', None)
    cache_timeout = laximo_settings.LAXIMO.get('CACHE_TIMEOUT', 0)
    key_prefix = laximo_settings.LAXIMO.get('CACHE_KEY_PREFIX', None)

    if cache_timeout and int(cache_timeout) > 0:
        return cache_page(
            cache_timeout, key_prefix=key_prefix, cache=cache_alias,
        )(view_func)

    return view_func


# todo settings.settings - мудачество!
if 'laximo' in settings.CACHES:
    cache = caches['laximo']
else:
    cache = caches['default']
