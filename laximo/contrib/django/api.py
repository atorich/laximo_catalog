# coding: utf-8


import logging

from requests import Timeout
from suds import WebFault

import laximo.oem.objects
from laximo import objects
from laximo.aftermarket import query as am_query
from laximo.contrib.django.exceptions import ServiceUnavailableException, LaximoHttpResponseFault, \
    CatalogCollisionException
from laximo.exceptions import MalformedResponse, LaximoRequestException
from laximo.oem import query as oem_query
from laximo.query import QuerySet
from laximo.request import OEMRequest, AftermarketRequest

"""
Модуль для инкапсуляции особенностей взаимодействия с библиотекой API Laximo
"""

logger = logging.getLogger(__name__)


def create_request(queryset):
    """
    Создает объект соответствующего queryset запроса (OEM или Aftermarket)
    :param queryset:
    :return:
    """
    query = queryset[0]

    if isinstance(query, oem_query.BaseOEMQuery):
        return OEMRequest(queryset)
    elif isinstance(query, am_query.BaseAMQuery):
        return AftermarketRequest(queryset)
    else:
        raise ValueError("Can't create request for queryset: %s" % queryset)


def request_laximo_api(queryset):
    """
    Посылает запрос в API Laximo, возвращает ответ
    В случае, если ответ содержит ошибку (Fault), бросает
    соответствующее исключение
    :param queryset:
    :return:
    """
    laximo_request = create_request(queryset)

    try:
        laximo_response = laximo_request.call()
    except (MalformedResponse, LaximoRequestException, Timeout, WebFault) as e:
        logger.error("Laximo Unavailable", extra={'exception': repr(e)})
        raise ServiceUnavailableException("Каталог временно недоступен")

    if laximo_response.is_fault:
        raise LaximoHttpResponseFault(fault=laximo_response.fault)

    return laximo_response


def get_catalog(value):
    """
    Запрашивает каталог по переданному значению авто
    Поиск осуществляется по полям:
        * code
        * brand
    Возвращает объект каталога в случае успеха, иначе None
    :param value:
    :return:
    """
    if not value:
        return None

    queryset = QuerySet(
        oem_query.ListCatalogs()
    )
    response = request_laximo_api(queryset)
    response_objects = get_laximo_objects(response)
    catalog_list = response_objects[laximo.oem.objects.CatalogList]

    def filter_func(catalog):
        return \
            catalog.code.lower() == value.lower() or \
            catalog.brand.lower() == value.lower() or \
            catalog.slug.lower() == value.lower()

    matched_catalog_list = list(filter(filter_func, catalog_list))

    if len(matched_catalog_list) > 1:
        raise CatalogCollisionException(
            'Возникла коллизия при поиске каталога по значению %s' % value
        )

    try:
        return matched_catalog_list[0]
    except IndexError:
        return None


def get_laximo_objects(laximo_response):
    """
    Создает из ответа от сервиса Laximo объекты моделей
    На каждый созданный экземпляр посылает соответствующий сигнал о создании
    """
    objects_dict = objects.objects_from_response(laximo_response)
    return objects_dict
