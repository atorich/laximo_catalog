# coding: utf-8
from functools import wraps

from django.core.cache import caches
from django.shortcuts import redirect
from django.utils.decorators import available_attrs
from ipware.ip import get_real_ip

from laximo.contrib.django.exceptions import RatelimitException
from laximo.contrib.django.settings import LAXIMO

cache = caches[LAXIMO.get('RATELIMIT_CACHE_BACKED', 'default')]
cache.clear()


def _process_ratelimit(request, limit=None, only_unauthenticated=True):
    if not limit:
        return False

    if only_unauthenticated and request.user.is_authenticated():
        return False

    key = "laximo_ratelimit_{ip}".format(ip=get_real_ip(request))
    value = cache.get(key, 0) + 1

    if value > limit:
        return True

    cache.set(key, value, LAXIMO.get('RATELIMIT_TIMEOUT'))


def ratelimit(**kwargs):
    limit = kwargs.get('limit', LAXIMO.get('RATELIMIT'))
    only_unauthenticated = kwargs.get('only_unauthenticated', LAXIMO.get('RATELIMIT_ONLY_UNAUTHENTICATED'))
    redirect_url = kwargs.get('redirect_url', LAXIMO.get('RATELIMIT_REDIRECT_URL'))
    
    def decorator(func):
        @wraps(func, assigned=available_attrs(func))
        def _wrapped_view(request, *view_args, **view_kwargs):
            if _process_ratelimit(request, limit, only_unauthenticated):
                if redirect_url:
                    return redirect(redirect_url)
                raise RatelimitException()
            return func(request, *view_args, **view_kwargs)

        return _wrapped_view

    return decorator
