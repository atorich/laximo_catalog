# coding: utf-8


from django.apps import AppConfig


class LaximoAppConfig(AppConfig):
    name = 'laximo.contrib.django'
    label = 'laximo'
    verbose_name = "Laximo Vehicle Catalog"

    def ready(self):
        from laximo.contrib.django.settings import setup_laximo_lib

        setup_laximo_lib()
