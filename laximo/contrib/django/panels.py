# coding: utf-8


import datetime
import logging
import threading
import xml.dom.minidom
from pyexpat import ExpatError

from debug_toolbar.panels import Panel
from debug_toolbar.panels.logging import (
    ThreadTrackingHandler, MESSAGE_IF_STRING_REPRESENTATION_INVALID
)
from debug_toolbar.utils import ThreadCollector
from django.utils.translation import ugettext_lazy as _, ungettext

from laximo.request import logger as req_logger

req_logger.setLevel(logging.DEBUG)


class LogCollector(ThreadCollector):
    def collect(self, item, thread=None):
        if 'laximo' not in item.get('channel', ''):
            return
        super(LogCollector, self).collect(item, thread)


class PanelLogHandler(ThreadTrackingHandler):
    def emit(self, record):
        try:
            message = record.getMessage()
        except Exception:
            message = MESSAGE_IF_STRING_REPRESENTATION_INVALID

        try:
            xml_string = xml.dom.minidom.parseString(record.msg)
            xml_message = xml_string.toprettyxml()
        except ExpatError:
            xml_message = None

        record = {
            'message': message,
            'xml': xml_message,
            'time': datetime.datetime.fromtimestamp(record.created),
            'level': record.levelname,
            'file': record.pathname,
            'line': record.lineno,
            'channel': record.name,
        }

        self.collector.collect(record)


collector = LogCollector()
logging_handler = PanelLogHandler(collector)
logging.root.addHandler(logging_handler)


class LaximoDebugPanel(Panel):
    name = 'Laximo'
    has_content = True

    template = 'laximo/debug/panel.html'

    nav_title = _("Laximo")

    title = _("Laximo debug messages")

    def __init__(self, *args, **kwargs):
        super(LaximoDebugPanel, self).__init__(*args, **kwargs)
        self._records = {}

    @property
    def nav_subtitle(self):
        records = self._records[threading.currentThread()]
        record_count = len(records)
        return ungettext("%(count)s message", "%(count)s messages",
                         record_count) % {'count': record_count}

    def process_request(self, request):
        collector.clear_collection()

    def generate_stats(self, request, response):
        records = collector.get_collection()
        self._records[threading.currentThread()] = records
        collector.clear_collection()
        self.record_stats({'records': records})
