# coding: utf-8


import re

from wrapt import ObjectProxy

from laximo.contrib.django.templatetags.laximo_tags import register, logger
from laximo.objects import IterableMixin
from laximo.utils import slugify


@register.filter
def objsort(items, prop_name):
    """
    Сортирует объекты по свойству ```prop_name```
    :param items:
    :param prop_name:
    :return:
    """
    return sorted(items, key=lambda o: getattr(o, prop_name))


@register.filter
def objrevsort(items, prop_name):
    """
    Сортирует объекты по свойству ```prop_name``` в обратном порядке
    :param items:
    :param prop_name:
    :return:
    """
    return sorted(items, key=lambda o: getattr(o, prop_name), reverse=True)


@register.filter
def safe_code(value, replace_with='_'):
    pattern = re.compile('[^a-zA-Z\d\s:]')
    return re.sub(pattern, '_', value)


@register.filter
def astree(obj, params):
    """
    Преобразовывает Iterable-объект, дочерние узлы которого не являются
    итерируемыми, но имеют некий parent_id идентификатор, например:
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListCategories
    для дальнейшего построения дерева через тег ```tree```

    Строка параметров разбивается по запятой на следующие переменные:
        children_prop: имя свойства для итерируемого
        контейнера (items, childrens, etc.)

        id_prop: имя свойства, содержащего id элемента
        parent_prop: имя свойства, содержащего parent_id элемента
        root_id: значение id, являющееся корнем дерева (None, '', 0, etc.)

    :param obj: итерируемый объект для преобразования
    :param params: строка параметров
    :return:
    """
    params = params.split(',')

    try:
        children_prop, id_prop, parent_prop, root_id = params
    except ValueError:
        children_prop, id_prop, parent_prop = params
        # Т.к. в приходящих от API данных формат parent_prop отличается
        # от каталога к каталогу, мы вынуждены определять parent_id
        # динамически - по первому элементу из списка.
        # NB! Предполагается, что первым всегда идет элемент, являющийся
        # корневой категорией
        root_id = getattr(obj.items[0], parent_prop) if obj.items else None

    class Node(IterableMixin, ObjectProxy):
        """
        Прокси-класс для узла
        """
        _iterable_prop = children_prop

        def __init__(self, *args, **kwargs):
            super(Node, self).__init__(*args, **kwargs)

            if not hasattr(self, self._iterable_prop):
                setattr(self, self._iterable_prop, [])

    nodes = []
    root_nodes = []
    lookup_map = {}

    t = Node(obj)

    for child in t:
        child = Node(child)
        child_id = getattr(child, id_prop)
        parent_id = getattr(child, parent_prop)

        lookup_map[child_id] = child
        nodes.append(child)

        if parent_id == root_id:
            root_nodes.append(child)

    for node in nodes:
        parent_id = getattr(node, parent_prop)

        if not parent_id == root_id:
            try:
                node_children = getattr(lookup_map[parent_id], children_prop)
            except KeyError:
                # Пример когда такое возможно:
                # https://app.getsentry.com/mikhail-kotov/carreta/issues/146129883/
                # http://wsdemo.laximo.ru/index.php?option=com_guayaquil&view=vehicle&c=AC1205&vid=25&ssd=$HQwXcmkMYXxwAXA4XDt_BgQNBgoFSDQlDHdhelZ_a1xOMxJY$&lang=ru
                # Если посмотреть XML, возвращаемый в этом случае,
                # можно заметить, что отсуттует узел с id=39
                # (<row categoryid="39" ...), тогда как узлы с параметром
                # parentcategory="39" - присутствуют.
                # В таких случаях логируем ошибку и позволяем дереву
                # строиться дальше
                logger.error(
                    "Undefined parent node with ID=%s" % parent_id
                )
            else:
                node_children += [node]
                setattr(lookup_map[parent_id], children_prop, node_children)

    setattr(t, children_prop, root_nodes)
    return t


@register.filter
def laximo_slugify(value):
    return slugify(value)
