# coding: utf-8


import logging
from collections import OrderedDict

from django import template
from django.core.urlresolvers import reverse
from django.template import TemplateSyntaxError
from django.template.base import Node
from django.template.defaulttags import url

from laximo.oem.objects import Unit
from laximo.utils import slugify

register = template.Library()
logger = logging.getLogger(__name__)


class CatalogURLNode(Node):
    def __init__(self, urlnode, parser, token):
        self.urlnode = urlnode
        self.parser = parser
        self.token = token
        self.catalog_expr = None
        self.ssd_expr = None
        self.prepare()

    def prepare(self):
        catalog_expr = self.urlnode.args.pop(0)
        brand_token = "%s.slug" % catalog_expr.token
        brand_expr = self.parser.compile_filter(brand_token)
        ssd_expr = self.urlnode.kwargs.pop('ssd', None)
        self.urlnode.args.insert(0, brand_expr)
        self.catalog_expr = catalog_expr
        self.ssd_expr = ssd_expr

    def render(self, context):
        catalog_object = self.catalog_expr.resolve(context)
        ssd = self.ssd_expr.resolve(context) if self.ssd_expr else None

        retval = self.urlnode.render(context)
        retval = "{}?catalog={}".format(retval, catalog_object.code)

        if catalog_object.is_wizard_available:
            retval = "{}&wizard=1".format(retval)

        if ssd:
            retval = "{}&ssd={}".format(retval, ssd)
        elif 'current_ssd' in context:
            retval = "{}&ssd={}".format(retval, context['current_ssd'])

        return retval


class TreeNode(template.Node):
    def __init__(self, tree, node_list):
        self.tree = tree
        self.node_list = node_list
        self.context = None

    @staticmethod
    def prepare(root):
        for item in root:
            yield item, [i for i in TreeNode.prepare(item)]

    def render(self, context):
        self.context = context
        tree = self.tree.resolve(context)
        prepared = [p for p in self.prepare(tree)]
        return "<ul class='root'>%s</ul>" % self.render_items(prepared, 0)

    def render_item(self, item, sub_items, level):
        inner_context = {
            'item': item,
            'level': level,
            'c': self.context,
        }
        return ''.join([
            '<li class="%s">' % ('leaf' if not sub_items else ''),
            item and self.node_list.render(
                template.Context(inner_context)) or '',
            sub_items and '<ul>%s</ul>' % ''.join(
                self.render_items(sub_items, level + 1)) or '',
            '</li>'
        ])

    def render_items(self, items, level):
        return ''.join(self.render_item(h, t, level) for h, t in items)


@register.simple_tag
def unit_imageurl(unit, size=None):
    if not size:
        size = 'ORIGINAL'
    size_value = getattr(Unit, 'IMAGE_SIZE_%s' % size)
    return unit.imageurl.replace('%size%', str(size_value))


@register.inclusion_tag('laximo/detail_attributes.html')
def detail_attributes(detail):
    return locals()


@register.simple_tag
def catalog_imageurl(catalog):
    # baseurl = "http://auto.vercity.ru/catalog/img/logo/auto/%s.png"
    baseurl = "/static/laximo/images/%s.png"

    if isinstance(catalog, str):
        brand = catalog
    else:
        brand = catalog.brand

    name = slugify(brand)

    return baseurl % name


@register.simple_tag
def details_or_filter_url(catalog, vehicle_id, unit_id, filter_str=None):
    """
    Возвращает ссылку на список деталей узла если не передан filter_str
    Если необходимо уточнение (передан filter_str), то возвращает ссылку
    на соответствующий фильтр
    :return:
    """
    if filter_str:
        retval = reverse('laximo:vehicle_unit_filter', args=(
            catalog.slug, vehicle_id, unit_id, filter_str
        ))
    else:
        retval = reverse('laximo:vehicle_unit_details', args=(
            catalog.slug, vehicle_id, unit_id
        ))

    return retval


@register.inclusion_tag('laximo/vehicle_list_table.html')
def vehicle_list_table(items, table_cls='', object_map=None):
    """
    Отображает список автомобилей
    Показывает те поля, которые перечислены в Vehicle.visible_attr_names
    :param items:
    :param table_cls:
    :param object_map:
    :return:
    """

    # т.к. в одном и том же наборе авто у объектов могут
    # присутствовать/отсутствовать разные поля, необходимо
    # пройтись по всему набору и вычленить список всех полей, доступных для
    # отображения
    # пример:
    # http://localhost:8000/new-catalog/land%20rover%20europe/vehicle/?catalog=LRE201412&wizard=1&ssd=$WiVATAt0AQA$&f=GetWizardForm

    header = OrderedDict()

    for item in items:
        header.update(item.visible_attrs)

    return locals()


@register.tag
def catalog_url(parser, token):
    """
    Возвращает ссылку на страницу каталога
    Работает аналогично стандартному тегу ```url```, но первым аргументом
    должен быть передан объект каталога, а так же присутствует возможность
    передать ssd через параметр kwargs (в этом случае, он не будет передан
    в ```reverse```)

    Используется для ЧПУ, в котором первым параметром в URL присутствует
    неизменный бренд авто вместо кода каталога, который может поменяться
    в любой момент

    В результирующей строке URL будет присутствовать querystring со следующими
    querystring-параметрами:
        catalog: берется из объекта каталога
        ssd: берется из kwargs или из ```current_ssd``` в контексте
        wizard: берется из свойства ```is_wizard_available``` объекта каталога

    Например:
        > {% catalog_url 'laximo:catalog_info' item %}
        >> /catalog/TOYOTA?catalog=TA201510&wizard=1

    :param parser:
    :param token:
    :return:
    """
    bits = token.split_contents()
    if len(bits) < 3:
        raise TemplateSyntaxError(
            "'%s' takes at least two arguments "
            "(path to a view and catalog object)" % bits[0]
        )
    urlnode = url(parser, token)
    return CatalogURLNode(urlnode, parser, token)


@register.tag
def tree(parser, token):
    """
    Используется для построения дерева в виде неупорядоченного списка (ul)
    из вложенных Iterable структур, например:
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListQuickGroup
    :param parser:
    :param token:
    :return:
    """
    bits = token.split_contents()
    if len(bits) != 2:
        raise template.TemplateSyntaxError(
            '"%s" takes one argument: iterable object' % bits[0]
        )
    node_list = parser.parse('end' + bits[0])
    parser.delete_first_token()
    return TreeNode(parser.compile_filter(bits[1]), node_list)
