# coding: utf-8
from django.contrib.auth.models import AnonymousUser, AbstractBaseUser
from django.core.cache import caches
from django.http import HttpResponseRedirect, HttpResponseNotAllowed
from django.test import TestCase, RequestFactory
from django.views.generic import View as BaseView

from laximo.contrib.django import settings
from laximo.contrib.django.views import RatelimitMixin
from laximo.contrib.django.exceptions import RatelimitException


class UnauthenticatedRateLimitedView(RatelimitMixin, BaseView):
    ratelimit_limit = 1
    ratelimit_only_unauthenticated = True
    ratelimit_redirect_url = None


class FullRateLimitedView(RatelimitMixin, BaseView):
    ratelimit_limit = 1
    ratelimit_only_unauthenticated = False
    ratelimit_redirect_url = None


class RedirectRateLimitedView(RatelimitMixin, BaseView):
    ratelimit_limit = 1
    ratelimit_only_unauthenticated = True
    ratelimit_redirect_url = '/'


class RatelimitTestCase(TestCase):
    def setUp(self):
        super(RatelimitTestCase, self).setUp()
        self.factory = RequestFactory()
        cache = caches[settings.LAXIMO.get('RATELIMIT_CACHE_BACKED', 'default')]
        cache.clear()

    def build_request(self, authenticated):
        request = self.factory.get('/')
        if authenticated:
            request.user = AbstractBaseUser()
        else:
            request.user = AnonymousUser()
        return request


class RatelimitOnlyAuthenticatedTestCase(RatelimitTestCase):
    """
    Тестирует случай, когда ходим во вьюху, зарезанную по правилу "все, кроме аутентифицированных" 
    """
    view_cls = UnauthenticatedRateLimitedView

    def test_authenticated_request_limit_not_exceeded_causes_ok(self):
        request = self.build_request(authenticated=True)

        self.view_cls.as_view()(request)
        response = self.view_cls.as_view()(request)
        self.assertIsInstance(response, HttpResponseNotAllowed)

    def test_unauthenticated_request_limit_exceeded_causes_fail(self):
        request = self.build_request(authenticated=False)

        self.view_cls.as_view()(request)
        with self.assertRaises(RatelimitException):
            self.view_cls.as_view()(request)


class RatelimitAllTestCase(RatelimitTestCase):
    """
    Тестирует случай, когда ходим во вьюху, зарезанную по правилу "все, включая аутентифицированных" 
    """
    view_cls = FullRateLimitedView

    def test_authenticated_request_limit_not_exceeded_causes_fail(self):
        request = self.build_request(authenticated=True)

        self.view_cls.as_view()(request)
        with self.assertRaises(RatelimitException):
            self.view_cls.as_view()(request)

    def test_unauthenticated_request_limit_exceeded_causes_fail(self):
        request = self.build_request(authenticated=False)

        self.view_cls.as_view()(request)
        with self.assertRaises(RatelimitException):
            self.view_cls.as_view()(request)


class RatelimitRedirectTestCase(RatelimitTestCase):
    """
    Тестирует корректность работы редиректа при попадании в лимит
    """
    view_cls = RedirectRateLimitedView

    def test_unauthenticated_request_limit_exceeded_causes_fail(self):
        request = self.build_request(authenticated=False)

        self.view_cls.as_view()(request)
        response = self.view_cls.as_view()(request)
        self.assertIsInstance(response, HttpResponseRedirect)
