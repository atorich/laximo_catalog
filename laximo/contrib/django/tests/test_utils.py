# coding: utf-8

import logging

from django.test import TestCase

import laximo.oem.objects
from laximo import objects
from laximo.contrib.django import utils

# fix https://github.com/nose-devs/nose/issues/795
logging.getLogger('suds.mx.core').setLevel(logging.ERROR)
logging.getLogger('suds.mx.literal').setLevel(logging.ERROR)
logging.getLogger('suds.wsdl').setLevel(logging.ERROR)
logging.getLogger('suds.xsd.schema').setLevel(logging.ERROR)
logging.getLogger('suds.xsd.query').setLevel(logging.ERROR)
logging.getLogger('suds.xsd.sxbase').setLevel(logging.ERROR)
logging.getLogger('suds.xsd.sxbasic').setLevel(logging.ERROR)
logging.getLogger('suds.metrics').setLevel(logging.ERROR)
logging.getLogger('suds.resolver').setLevel(logging.ERROR)
logging.getLogger('suds.client').setLevel(logging.ERROR)


class GetVehicleCatalogTestCase(TestCase):
    XML_OBJECT = """
    <row xmlns="http://WebCatalog.Kito.ec" brand="SUBARU" catalog="SLR1303" name="FORESTER" ssd="$HSUzdAJnfQt_YANSIkkECg0EB01ddlpVSwFNa3V4RFt8MxA9dHdTQhM$" vehicleid="1023">
        <attribute key="framecolor" name="Цвет кузова" value="32J (OBSIDIAN BLACK PEARL)"/>
        <attribute key="mainoptioncode" name="mainoptioncode" value="2S"/>
        <attribute key="date" name="Дата выпуска" value="07.01.2009"/>
        <attribute key="destinationregion" name="Для региона" value="LH"/>
        <attribute key="destinationcode" name="destinationcode" value="EA"/>
        <attribute key="modelyearto" name="Модель выпускается по" value="10.2012"/>
        <attribute key="modification" name="Модификация" value="SH9-L9T"/>
        <attribute key="modelyearfrom" name="Модель выпускается с" value="12.2007"/>
        <attribute key="transmission" name="КП" value="4AT"/>
        <attribute key="engine" name="Двигатель" value="255"/>
        <attribute key="grade" name="Комплектация" value="XT"/>
        <attribute key="modelcode" name="modelcode" value="S12"/>
        <attribute key="dateto" name="Выпуск по" value="11.2010"/>
        <attribute key="model" name="Модель" value="SH9AL9T"/>
        <attribute key="datefrom" name="Выпуск с" value="12.2007"/>
        <attribute key="trimcolor" name="Цвет салона" value="H20"/>
        <attribute key="frame" name="Кузов" value="W"/>
        <attribute key="train" name="train" value="4W"/>
    </row>
    """

    def setUp(self):
        super(GetVehicleCatalogTestCase, self).setUp()
        self.obj = objects.object_from_xml_string(
            laximo.oem.objects.Vehicle, self.XML_OBJECT
        )
        self.assertIsNotNone(self.obj)

    def test_catalog_resolved_by_vehicle_object_ok(self):
        catalog = utils.get_vehicle_catalog(self.obj)
        self.assertEquals(catalog.name, 'Subaru Europe')
