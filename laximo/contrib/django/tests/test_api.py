# coding: utf-8

from unittest import TestCase as UTestCase

from laximo import request
from laximo.aftermarket import query as am_query
from laximo.contrib.django.api import create_request
from laximo.oem import query as oem_query
from laximo.query import QuerySet


class CreateRequestTestCase(UTestCase):
    class TestOEMQuery(oem_query.BaseOEMQuery):
        pass

    class TestAMQuery(am_query.BaseAMQuery):
        pass

    def setUp(self):
        super(CreateRequestTestCase, self).setUp()

    def test_oem_created_ok(self):
        queryset = QuerySet(self.TestOEMQuery())
        req = create_request(queryset)
        self.assertIsInstance(req, request.OEMRequest)

    def test_am_created_ok(self):
        queryset = QuerySet(self.TestAMQuery())
        req = create_request(queryset)
        self.assertIsInstance(req, request.AftermarketRequest)
