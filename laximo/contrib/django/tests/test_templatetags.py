# coding: utf-8


from unittest import TestCase

from laximo.contrib.django.templatetags.laximo_filters import astree
from laximo.objects import IterableMixin


class TestItem(object):
    _counter = 0

    def __init__(self, parent=None):
        self.id = TestItem._counter + 1
        self.parent = parent
        TestItem._counter += 1


class TestList(IterableMixin):
    def __init__(self, items=None):
        self.list_test_prop = None
        self.items = items or []


class AsTreeTestCase(TestCase):
    def setUp(self):
        self.list = TestList(
            [
                TestItem(),  # id=1
                TestItem(),  # id=2
                TestItem(),  # id=3

                TestItem(parent=3),  # id=4, parent=3
                TestItem(parent=3),  # id=5, parent=3

                TestItem(parent=4),  # id=6, parent=4
                TestItem(parent=4),  # id=7, parent=4

                TestItem(parent=5),  # id=8, parent=5
                TestItem(parent=5),  # id=9, parent=5
                TestItem(parent=5),  # id=10, parent=5
            ]
        )

    def test_astree_builds_tree(self):
        tree = astree(self.list, 'items,id,parent')
        self.assertEquals(len(tree.items), 3)

        self.assertEquals(len(tree.items[0].items), 0)
        self.assertEquals(len(tree.items[1].items), 0)
        self.assertEquals(len(tree.items[2].items), 2)

        self.assertEquals(tree.items[2].items[0].id, 4)
        self.assertEquals(len(tree.items[2].items[0].items), 2)

        self.assertEquals(tree.items[2].items[1].id, 5)
        self.assertEquals(len(tree.items[2].items[1].items), 3)
