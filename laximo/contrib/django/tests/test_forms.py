# coding=utf-8


import django

from laximo.contrib.django import forms
from laximo.contrib.django.tests.base import TestCase


django.setup()


class TestQuery(object):
    def __init__(self, first, second=None, third=False):
        pass


class TestFieldsBuilder(forms.BaseFieldsBuilder):
    pass


class QueryFormFayesctoryTestCase(TestCase):
    def setUp(self):
        self.factory = forms.QueryFormFactory(
            query_cls=TestQuery, fields_builder=TestFieldsBuilder)

    def test_get_query_args(self):
        """
        Тестирует, что аргументы инициализатора объекта (в данном случае
        TestQuery) выбираются корректно
        :return:
        """
        args = self.factory.get_query_args()[0]
        self.assertEquals(len(args), 3)
        self.assertIn('first', args)
        self.assertIn('second', args)
        self.assertIn('third', args)

    def test_form_fields_created_ok(self):
        """
        Тестирует, что поля формы создаются корректно
        :return:
        """
        form_cls = self.factory.create()
        form = form_cls()
        self.assertIn('first', form.fields)
        self.assertIn('second', form.fields)
        self.assertIn('third', form.fields)
        self.assertIn('ssd', form.fields)

    def test_required_fields(self):
        """
        Тестируют, что свойство required полей формы определяется корректно
        :return:
        """
        form_cls = self.factory.create()
        form = form_cls()

        first = form.fields.get('first')
        self.assertTrue(first.required)

        second = form.fields.get('second')
        self.assertFalse(second.required)

        third = form.fields.get('third')
        self.assertFalse(third.required)

        ssd = form.fields.get('ssd')
        self.assertFalse(ssd.required)


class QueryFormFactoryExcludeTestCase(TestCase):
    """
    Тестирует, что при передаче в фабрику параметра ```exclude```
    поля с данными именами не создаются
    """

    def test_form_excluded_fields(self):
        form_cls = forms.QueryFormFactory(
            query_cls=TestQuery, fields_builder=TestFieldsBuilder,
            exclude_query_args=['second']
        ).create()
        form = form_cls()

        first = form.fields.get('first')
        self.assertTrue(first.required)

        second = form.fields.get('second')
        self.assertIsNone(second)

        third = form.fields.get('third')
        self.assertFalse(third.required)

        ssd = form.fields.get('ssd')
        self.assertFalse(ssd.required)

    def test_form_exclude_required_field(self):
        form_cls = forms.QueryFormFactory(
            query_cls=TestQuery, fields_builder=TestFieldsBuilder,
            exclude_query_args=['first']
        ).create()
        form = form_cls()

        first = form.fields.get('first')
        self.assertIsNone(first)

        second = form.fields.get('second')
        self.assertFalse(second.required)

        third = form.fields.get('third')
        self.assertFalse(third.required)

        ssd = form.fields.get('ssd')
        self.assertFalse(ssd.required)


class QueryFormFactoryExtraTestCase(TestCase):
    """
    Тестирует передачу параметра ```extra``` в фабрику форм
    """
    def setUp(self):
        self.factory = forms.QueryFormFactory(
            query_cls=TestQuery,
            fields_builder=TestFieldsBuilder,
            extra={
                'first': {
                    'label': 'The first one',
                },
                'second': {
                    'help_text': 'Second help text'
                }
            }
        )

    def test_form_labels_exists(self):
        """
        Тестирует, что свойство label устанавливается корректно
        :return:
        """
        form_cls = self.factory.create()
        form = form_cls()
        first = form.fields.get('first')
        self.assertEquals(first.label, 'The first one')

    def test_form_help_text_exists(self):
        """
        Тестирует, что свойство help_text устанавливается корректно
        :return:
        """
        form_cls = self.factory.create()
        form = form_cls()
        first = form.fields.get('second')
        self.assertEquals(first.help_text, 'Second help text')
