# coding: utf-8

import logging

import django
from django.core.urlresolvers import reverse
from suds.transport import Reply

import laximo.oem.objects
from laximo.contrib.django.tests.base import TestCase
from laximo.tests.test_response import FakeMixin

django.setup()

# fix https://github.com/nose-devs/nose/issues/795
logging.getLogger('suds.mx.core').setLevel(logging.ERROR)
logging.getLogger('suds.mx.literal').setLevel(logging.ERROR)
logging.getLogger('suds.wsdl').setLevel(logging.ERROR)
logging.getLogger('suds.xsd.schema').setLevel(logging.ERROR)
logging.getLogger('suds.xsd.query').setLevel(logging.ERROR)
logging.getLogger('suds.xsd.sxbase').setLevel(logging.ERROR)
logging.getLogger('suds.xsd.sxbasic').setLevel(logging.ERROR)
logging.getLogger('suds.metrics').setLevel(logging.ERROR)
logging.getLogger('suds.resolver').setLevel(logging.ERROR)
logging.getLogger('suds.client').setLevel(logging.ERROR)


class TestLaximo5XXResponseTestCase(FakeMixin, TestCase):
    url = reverse('laximo:catalog_info', args=('TOYOTA',))

    def test_nginx_response_catched_ok(self):
        html = """<html>
           <head><title>502 Bad Gateway</title></head>
           <body bgcolor="white">
           <center><h1>502 Bad Gateway</h1></center>
           <hr><center>nginx/1.8.1</center>
           </body>
           </html>
        """
        reply = Reply(500, {}, html)
        self.mock(reply)
        rv = self.client.get(
            "%s?catalog=TA201511&wizard=1" % self.url
        )
        self.assertEqual(rv.status_code, 503)
        self.unmock()


class CatalogInfoTestCase(TestCase):
    url = reverse('laximo:catalog_info', args=('TOYOTA',))

    def test_object_in_context(self):
        rv = self.client.get(
            "%s?catalog=TA201511&wizard=1" % self.url
        )
        self.assertIn('object', rv.context)
        self.assertIsInstance(rv.context['object'],
                              laximo.oem.objects.CatalogInfo)


class CatalogInfoWithWizardTestCase(TestCase):
    url = reverse('laximo:catalog_info', args=('TOYOTA',))

    def test_wizard_form_is_in_context(self):
        """
        Тестирует, что форма GetWizardForm присутствует в наборе форм
        в контексте
        :return:
        """
        rv = self.client.get(
            "%s?catalog=TA201511&wizard=1" % self.url
        )
        self.assertIn('GetWizardForm', rv.context['form'])

    def test_wizard_form_has_proper_fields(self):
        """
        Тестирует, что что экземпляр формы создается корректно
        (через WizardMixin)
        :return:
        """
        rv = self.client.get(
            "%s?catalog=TA201511&wizard=1" % self.url
        )
        form_cls = rv.context['form']['GetWizardForm']
        form = form_cls()
        self.assertIn('Model', form.fields)
        self.assertIn('Frame', form.fields)
        self.assertIn('Date', form.fields)

    def test_wizard_param_with_unavailable_wizard(self):
        # with unavailable wizard
        url = reverse('laximo:catalog_info', args=('ABARTH',))
        rv = self.client.get(
            "%s?catalog=TFiat112013&wizard=1" % url
        )

        self.assertEquals(rv.status_code, 200)


class WizardViewTestCase(TestCase):
    url = reverse('laximo:wizard', args=('TOYOTA',))

    def test_wizard_form_is_in_context(self):
        """
        Тестирует, что форма GetWizardForm присутствует в наборе форм
        в контексте
        :return:
        """
        rv = self.client.get(
            "%s?catalog=TA201511&ssd=$WiVLCwYN$" % self.url
        )
        self.assertIn('GetWizardForm', rv.context['form'])


class FindVehicleWithoutFormName(TestCase):
    """
    Тестирует случай, когда пришел запрос в catalog_vehicle_list, но форма,
    по которой мы сюда попали не была по какой-то причине
    передана в параметры запроса
    """
    base_url = reverse('laximo:catalog_vehicle_list', args=('TA201511',))

    def get_url(self):
        return "%s?vin=JTMHX02J404107033" % self.base_url

    def test_response_400(self):
        rv = self.client.get(
            self.get_url()
        )
        self.assertEquals(rv.status_code, 400)


class FindVehicleByVINTestCase(TestCase):
    base_url = reverse('laximo:catalog_vehicle_list', args=('TA201511',))

    def get_url(self):
        return "%s?vin=JTMHX02J404107033&" \
               "f=FindVehicleByVINForm" % self.base_url

    def test_response_ok(self):
        """
        Тестирует, что форма GetWizardForm присутствует в наборе форм
        в контексте
        :return:
        """
        rv = self.client.get(
            self.get_url()
        )
        self.assertEquals(rv.status_code, 200)


class FindVehicleByWizardTestCase(TestCase):
    base_url = reverse('laximo:catalog_vehicle_list', args=('BRAND',))

    def get_url(self):
        return "%s?catalog=TA201511&ssd=$WiVLCwYN$&f=GetWizardForm" \
               % self.base_url

    def test_response_ok(self):
        """
        Тестирует, что форма GetWizardForm присутствует в наборе форм
        в контексте
        :return:
        """
        rv = self.client.get(
            self.get_url()
        )
        self.assertEquals(rv.status_code, 200)


class FindVehicleByFrameTestCase(TestCase):
    base_url = reverse('laximo:catalog_vehicle_list', args=('MAZDA201510',))

    def get_url(self):
        return "%s?frame=SGL5&frameno=400683&ssd=&" \
               "f=FindVehicleByFrameForm" % self.base_url

    def test_response_ok(self):
        """
        Тестирует, что форма GetWizardForm присутствует в наборе форм
        в контексте
        :return:
        """
        rv = self.client.get(
            self.get_url()
        )
        self.assertEquals(rv.status_code, 200)


class TopSearchFindVehicleByFrameTestCase(TestCase):
    baseurl = reverse('laximo:vehicle_list')

    def get_url(self):
        return "%s?frame=&frameno=" \
               "&frame_full=ACR50-0195054" \
               "&f=FindVehicleByFrameForm" % self.baseurl

    def test_response_ok(self):
        rv = self.client.get(self.get_url())
        self.assertEquals(rv.status_code, 200)


class FindVehicleByFrameBadRequestTestCase(TestCase):
    base_url = reverse('laximo:catalog_vehicle_list', args=('MAZDA201510',))

    def test_request_with_none_frame(self):
        url = "%s?frame=&frameno=400683&ssd=&" \
              "f=FindVehicleByFrameForm" % self.base_url
        rv = self.client.get(url)
        self.assertEquals(rv.status_code, 200)

    def test_request_with_none_frameno(self):
        url = "%s?frame=SGL5&frameno=&ssd=&" \
              "f=FindVehicleByFrameForm" % self.base_url
        rv = self.client.get(url)
        self.assertEquals(rv.status_code, 200)

    def test_request_with_bad_frameno(self):
        url = "%s?frame=SGL5&frameno=dsdsd&ssd=&" \
              "f=FindVehicleByFrameForm" % self.base_url
        rv = self.client.get(url)
        self.assertEquals(rv.status_code, 200)


class FindVehicleByModelCodeTestCase(TestCase):
    base_url = reverse('laximo:catalog_vehicle_list', args=('BRAND',))

    def test_response_ok(self):
        url = "%s?catalog=NIS201504&modelCode=280ZX&ssd=&" \
              "operation=searchByModelCode&f=ExecCustomOperationForm" \
              % self.base_url

        rv = self.client.get(url)
        self.assertEquals(rv.status_code, 200)

    def test_invalid_form_response_ok(self):
        url = "%s?catalog=NIS201504&modelCode=12&ssd=" \
              "&operation=searchByModelCode&" \
              "f=ExecCustomOperationForm" % self.base_url
        rv = self.client.get(url)
        self.assertEquals(rv.status_code, 200)
        form = rv.context['form']['ExecCustomOperationForm']
        self.assertFalse(form.is_valid())


class VehicleQuickGroupsTestCase(TestCase):
    base_url = reverse('laximo:vehicle_quickgroups', args=('TOYOTA', '9844'))

    def get_url(self):
        return "%s?catalog=TA201511" % self.base_url

    def test_response_ok(self):
        """
        Тестирует, что форма GetWizardForm присутствует в наборе форм
        в контексте
        :return:
        """
        rv = self.client.get(
            self.get_url()
        )
        self.assertEquals(rv.status_code, 200)

    def test_response_context_has_vehicle(self):
        rv = self.client.get(
            self.get_url()
        )
        vehicle = rv.context['object_map']['Vehicle']
        self.assertIsInstance(vehicle, laximo.oem.objects.Vehicle)

    def test_response_context_has_quickgroups(self):
        rv = self.client.get(
            self.get_url()
        )
        vehicle = rv.context['object']
        self.assertIsInstance(vehicle, laximo.oem.objects.QuickGroupTree)


class VehicleQuickDetailsTestCase(TestCase):
    ssd = '$HQwdcgEFDQQFB0g0JU8Bahl3X1RjVzIkWA$'
    base_url = reverse('laximo:vehicle_quickdetails',
                       args=('TOYOTA', '218663', '11749'))

    def get_url(self):
        return "%s?catalog=TA201511&ssd=%s" % (self.base_url, self.ssd)

    def test_response_ok(self):
        """
        Тестирует, что форма GetWizardForm присутствует в наборе форм
        в контексте
        :return:
        """
        rv = self.client.get(
            self.get_url()
        )
        self.assertEquals(rv.status_code, 200)

    def test_response_context_has_vehicle(self):
        rv = self.client.get(
            self.get_url()
        )
        vehicle = rv.context['object_map']['Vehicle']
        self.assertIsInstance(vehicle, laximo.oem.objects.Vehicle)

    def test_response_context_has_quickdetails(self):
        rv = self.client.get(
            self.get_url()
        )
        vehicle = rv.context['object']
        self.assertIsInstance(vehicle, laximo.oem.objects.QuickDetailTree)


class UnitListViewTestCase(TestCase):
    """
    Тестирует поиск по изображениям с построением дерева категорий агрегатов
    """
    base_url = reverse('laximo:vehicle_unit_list',
                       args=('RENAULT', '1280'))

    def get_url(self, ssd, category_id=None):
        url = "%s?catalog=RENAULT201507&ssd=%s" % (self.base_url, ssd)

        if not category_id:
            return url

        return "%s&category_id=%s" % (url, category_id)

    def test_root_category_response_ok(self):
        """
        Тестирует отображение изображений в коревой категории
        :return:
        """
        rv = self.client.get(
            self.get_url(ssd="$BQoJQHMFBwoDSlgPHTkCTVpaQkFmWQQROQMBDAVMRV1Y"
                             "KzM_A3h3BX9wcgRcXUsFBQAMTGx0Rxk6DFpGeGB9RnUX$")
        )
        self.assertEquals(rv.status_code, 200)

    def test_subcategory_response_ok(self):
        """
        Тестирует отображение изображений во вложенной категории
        :return:
        """
        rv = self.client.get(
            self.get_url(
                category_id=75629,
                ssd="$BQoJQHMFBwoDSkYKFxxcR3R4AQBKWA8dOQJNWlpCQWZZBBE5AwEMBU"
                    "xeUVgeBhZWVmZaXUd0e1hWB0RaWnVkdQV9KFU0cXUGAgoBAwBfXAdtc"
                    "0VHbUZcQycwNkdyFUtRQXQH$"
            )
        )
        self.assertEquals(rv.status_code, 200)

    def test_unit_list_item_has_filter_response_ok(self):
        """
        Тестирует, что страница с узлами, для которых требуется уточнение
        характеристик автомобиля отображается корректно

        См. http://technologytrade.ru/index.php/Laximo_Web-services:OEM:GetFilterByUnit
        :return:
        """
        rv = self.client.get(
            self.get_url(
                category_id=75629,
                ssd="$BQoJQHMFBwoDSkYKFxxcR3R4AQBKWA8dOQJNWlpCQWZZBBE5AwEMBUx"
                    "eUVgeBhZWVmZaXUd0e1hWB0RaWnVkdQV9KFU0cXUGAgoBAwBfXAdtc0V"
                    "HbUZcQycwNkdyFUtRQXQH$"
            )
        )
        self.assertEquals(rv.status_code, 200)


class VehicleUnitFilterViewTestCase(TestCase):
    base_url = reverse('laximo:vehicle_unit_filter', args=(
        'RENAULT', '1280', '6264',
        'JENBUEAxMjgwQNCj0LLQtdC70LjRh9C10L3QvdCw0Y8g0L-QvtC70LXQt9C90LDRjyDQv'
        'dCw0LPRgNGD0LfQutCwI9Ch0YLQsNC90LTQsNGA0YLQvdCw0Y8g0L-QvtC70LXQt9C90L'
        'DRjyDQvdCw0LPRgNGD0LfQutCwI9Ch0YLQsNC90LTQsNGA0YLQvdCw0Y8g0L-QvtC70LX'
        'Qt9C90LDRjyDQvdCw0LPRgNGD0LfQutCwICs_'
    ))

    def test_response_ok(self):
        ssd = "$BQoJQHMFBwoDSkYKFxxcR3R4AQBKQwIBOQdNXVhVcwQGW1NMCwsCS1xXTHZbGx" \
              "ddQ0ZnXVxAdlpXQQJNWVBcRldZDwA5fwAHBgFxSlsOCwxRXFBQYFxbQisoSgFNQ" \
              "lxcc2JwWi46An53cwAEDARcU00LTUFWXVdRdiVWSgYDBQVMbHRHGToMWkZ4YH1G" \
              "dRcVBgtyAg$"

        rv = self.client.get(
            "%s?catalog=RENAULT201507&ssd=%s" % (
                self.base_url,
                ssd
            )
        )

        self.assertEquals(rv.status_code, 200)


class ListQuickGroupsNotSupportedTestCase(TestCase):
    """
    Тестирует случай, когда при выборе авто недоступен список деталей
    (ListQuickGroups) и в этом случае вместо ошибки LaximoHttpResponseFault
    мы должны попытаться перенаправить пользователя на поиск по изображениям

    Пример:
    у нас
    http://localhost:8000/catalog/vehicle/?f=FindVehicleByVINForm&vin=XUUNF486J80030811

    на wsdemo
    http://wsdemo.laximo.ru/index.php?option=com_guayaquil&amp;view=vehicles&amp;ft=findByVIN&amp;c=ch1106&amp;vid=47469&amp;ssd=&amp;vin=XUUNF486J80030811&amp;frame=&amp;frameNo=&amp;Itemid=512&amp;lang=ru
    """

    base_url = reverse('laximo:vehicle_quickgroups', args=(
        'CHEVROLET', '47469'
    ))

    def get_url(self):
        return "%s?catalog=ch1106&" \
               "ssd=$HQwXcmthYHx1AA5dL0ECAwcFCgIFSAcKGlNfUXVARmtkPhsmclBB" \
               "GGVKfE9bFxpREg$" % self.base_url

    def test_response_ok(self):
        """
        Тестирует, что форма GetWizardForm присутствует в наборе форм
        в контексте
        :return:
        """
        rv = self.client.get(
            self.get_url()
        )
        self.assertEquals(rv.status_code, 302)
