# coding: utf-8

from django.test import TestCase as BaseTestCase


class TestCase(BaseTestCase):
    """
    https://django-nose.readthedocs.org/en/latest/usage.html#enabling-database-reuse
    """
    cleans_up_after_itself = True
