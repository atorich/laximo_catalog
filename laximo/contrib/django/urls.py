from django.conf.urls import patterns, url

from laximo.contrib.django import views
from laximo.contrib.django.cache import cache_catalog_page

urlpatterns = patterns(
    '',
    url(r'^$', views.CatalogListView.as_view(), name='catalog_list'),

    url(r'^(?P<brand>[A-z0-9\s\-]+)/wizard/$',
        cache_catalog_page(views.WizardView.as_view()),
        name='wizard'),

    url(r'^vehicle/$',
        cache_catalog_page(views.VehicleListView.as_view()),
        name='vehicle_list'),

    url(r'^(?P<brand>[A-z0-9\s\-]+)/vehicle/$',
        cache_catalog_page(views.VehicleListView.as_view()),
        name='catalog_vehicle_list'),

    url(r'^(?P<brand>[A-z0-9\s\-]+)/$',
        cache_catalog_page(views.CatalogInfoView.as_view()),
        name='catalog_info'),

    url(
        r'^(?P<brand>[A-z0-9\s\-]+)/vehicle/(?P<vehicle_id>[A-z0-9]+)/groups/$',
        cache_catalog_page(views.VehicleQuickGroupsView.as_view()),
        name='vehicle_quickgroups'),

    url(r'^(?P<brand>[A-z0-9\s\-]+)/vehicle/(?P<vehicle_id>[A-z0-9]+)/units/$',
        cache_catalog_page(views.VehicleUnitListView.as_view()),
        name='vehicle_unit_list'),

    url(r'^(?P<brand>[A-z0-9\s\-]+)/vehicle/(?P<vehicle_id>[A-z0-9]+)/groups/'
        r'(?P<group_id>[A-z0-9]+)/details/$',
        cache_catalog_page(views.VehicleQuickDetailsView.as_view()),
        name='vehicle_quickdetails'),

    url(r'^(?P<brand>[A-z0-9\s\-]+)/vehicle/(?P<vehicle_id>[A-z0-9]+)/units/'
        r'(?P<unit_id>[A-z0-9]+)/(?P<filter>[0-9A-z\$\-\_*]+)/$',
        cache_catalog_page(views.VehicleUnitFilterView.as_view()),
        name='vehicle_unit_filter'),

    url(r'^(?P<brand>[A-z0-9\s\-]+)/vehicle/(?P<vehicle_id>[A-z0-9]+)/units/'
        r'(?P<unit_id>[A-z0-9]+)/$',
        cache_catalog_page(views.VehicleUnitDetails.as_view()),
        name='vehicle_unit_details'),
)
