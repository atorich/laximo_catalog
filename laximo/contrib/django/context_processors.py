# coding: utf-8


from laximo.contrib.django import forms as laximo_forms

vinsearch_form_cls = laximo_forms.build_query_form(
    meta=laximo_forms.meta_map['vinsearch']
)

framesearch_form_cls = laximo_forms.build_query_form(
    meta=laximo_forms.meta_map['framesearch']
)


def search_forms(request):
    f = request.GET.get('f', None)

    vinsearch_form = vinsearch_form_cls()
    framesearch_form = framesearch_form_cls()

    if f == vinsearch_form.name:
        vinsearch_form = vinsearch_form_cls(request.GET)
    elif f == framesearch_form.name:
        framesearch_form = framesearch_form_cls(request.GET)

    forms = {
        'vinsearch_form': vinsearch_form,
        'framesearch_form': framesearch_form,
    }

    return forms
