# coding: utf-8


import logging

import laximo.oem.objects
from laximo.contrib.django import api

logger = logging.getLogger(__name__)


def get_catalog_operation(catalog, request):
    """
    Возвращает запрашиваемый в рамках текущего запроса CatalogOperation,
    поддерживаемый данным каталогом

    :param catalog: CatalogInfo
    :param request: django request object
    :return:
    """
    operation = request.GET.get('operation')

    if not operation:
        return None

    matched_operations = [o for o in catalog.extensions.operations if o.name == operation]

    try:
        return matched_operations[0]
    except IndexError:
        return None


def get_catalog_feature(catalog, feature_name):
    """
    Возвращает запрашиваемый CatalogFeature, поддерживаемый данным каталогом
    :param catalog: CatalogInfo
    :param feature_name: feature_name см. objects.CatalogInfo
    http://laximo.net/index.php/Laximo_Web-services:OEM:GetCatalogInfo
    :return:
    """
    for feature in catalog.features:
        if feature.name == feature_name:
            return feature
    return None


def get_vehicle_catalog(obj):
    """
    Возвращает каталог, к которому относятся объекты авто из
    параметра ```obj```.

    В ```obj``` может быть передан результат методов
    ```FindVehicleByVIN```, ```FindVehicleByFrame``` или
    ```FindVehicleByWizard```, а так же словарь с ответом от API Laximo,
    где присутствуют данные объекты

    :param obj:
    :return:
    """
    if isinstance(obj, dict):
        obj = \
            obj.get(laximo.oem.objects.FindVehicleByVIN) or \
            obj.get(laximo.oem.objects.FindVehicleByFrame) or \
            obj.get(laximo.oem.objects.FindVehicleByWizard)

    if isinstance(obj, (
            laximo.oem.objects.FindVehicleByVIN,
            laximo.oem.objects.FindVehicleByFrame,
            laximo.oem.objects.FindVehicleByWizard
    )):
        try:
            vehicle = obj.items[0]
        except IndexError:
            return None
    elif isinstance(obj, laximo.oem.objects.Vehicle):
        vehicle = obj
    else:
        return None

    value = vehicle.catalog or vehicle.brand
    catalog = api.get_catalog(value)
    return catalog
