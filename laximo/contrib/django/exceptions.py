# coding: utf-8


class LaximoHttpResponseFault(Exception):
    def __init__(self, *args, **kwargs):
        self.fault = kwargs.pop('fault', None)
        super(LaximoHttpResponseFault, self).__init__(*args, **kwargs)


class BadRequestParamsException(Exception):
    def __init__(self, *args, **kwargs):
        self.form = kwargs.pop('form', None)


class NotFoundException(Exception):
    def __init__(self, *args, **kwargs):
        self.queryset = kwargs.pop('queryset', None)


class ServiceUnavailableException(Exception):
    """Объединяет 502, 503, 504"""
    pass


class UnknownCatalog(ValueError):
    pass


class CatalogCollisionException(Exception):
    pass


class RatelimitException(Exception):
    pass
