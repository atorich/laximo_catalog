# coding=utf-8


from xml.sax import saxutils

from django.utils.encoding import force_text
from lxml import etree

from laximo import settings
from laximo.errors import ERROR_MESSAGES
from laximo.exceptions import MalformedResponse
from laximo.utils import xpath_ns


class Response(object):
    """Объект ответа от сервиса"""

    def __init__(self, request, raw_data):
        self.request = request

        # на состояние 12.04.2016 из API Laximo внутри ответа возвращаются
        # заэскейпенные символы '<', поэтому xml имеет вид
        # &ltresponse>...&ltresponse/>
        # для исправления этого поведения необходимо их анэскейпить
        if settings.UNESCAPE_RESPONSE_DATA:
            self.data = saxutils.unescape(raw_data.decode('utf8'))
        else:
            self.data = raw_data
        self.data = force_text(self.data)

        self.validate_data()
        self.tree = etree.fromstring(self.data)
        self.fault = self.get_fault()

    def validate_data(self):
        if self.data.strip().startswith('<html>'):
            raise MalformedResponse("Got html instead xml")

    @property
    def is_fault(self):
        return self.fault is not None

    def get_fault(self):
        fault = xpath_ns(self.tree, '//Fault') or None

        if not fault:
            return None

        faultcode, faultstring = None, None

        try:
            faultcode = fault[0].xpath('faultcode')[0].text
        except KeyError:
            pass

        try:
            faultstring = fault[0].xpath('faultstring')[0].text
        except KeyError:
            pass

        return Fault(faultcode, faultstring)


class Fault(object):
    """Описывает ошибку, приходящую от сервиса"""

    def __init__(self, faultcode, faultstring):
        self.faultcode = faultcode
        self.faultstring = faultstring
        self.fault_info = self.faultstring.split(':')

    @property
    def code(self):
        try:
            return self.fault_info[0]
        except IndexError:
            return None

    @property
    def message(self):
        if self.code not in ERROR_MESSAGES:
            return self.faultstring

        message = ERROR_MESSAGES.get(self.code)

        try:
            return message.format(self.fault_info[1])
        except IndexError:
            return self.faultstring
