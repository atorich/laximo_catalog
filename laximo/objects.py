# coding=utf-8


import logging


import lxml
from dexml import fields, ParseError

from laximo.exceptions import LaximoException
from laximo.utils import xpath_ns

logger = logging.getLogger(__name__)


class LabeledString(fields.String):
    def __init__(self, **kwargs):
        self.verbose_name = kwargs.pop('verbose_name', None)
        super(LabeledString, self).__init__(**kwargs)


class IterableMixin(object):
    _iterable_prop = 'items'

    def __iter__(self):
        return iter(getattr(self, self._iterable_prop))


def object_from_xml_string(object_cls, xml_str):
    """
    Создает объект из XML-данных

    :param object_cls: класс создаваемого объекта
    :param xml_str: XML
    :return:
    """
    try:
        obj = object_cls.parse(xml_str)
    except ParseError as e:
        message = "%s\nXML: %s" % (e.message, xml_str)
        logger.error(message)
        raise LaximoException(message, original_exc=e)

    return obj


def object_from_xml_element(object_cls, element):
    """
    Создает объект из экземпляра lxml Element
    :param object_cls: класс создаваемого объекта
    :param element: lxml Element
    :return:
    """
    try:
        xml_str = lxml.etree.tostring(element)
    except TypeError as e:
        # todo: wtf? в каком случае такое возникает?
        logger.error(e.message)
        return None

    return object_from_xml_string(object_cls, xml_str)


def objects_from_response(response):
    """
    Создает объекты из экземпляра ответа от сервиса ```laximo.Response```

    :param response: экземпляр Response
    :return:
    """
    if not response.data:
        return []

    def mapper(args):
        object_cls, selector = args
        try:
            element = xpath_ns(response.tree, selector)[0]
        except IndexError as e:
            return object_cls, None

        return object_cls, object_from_xml_element(object_cls, element)

    objects_map = get_response_objects_map(response)
    return dict(list(map(mapper, iter(list(objects_map.items())))))


def object_from_response(response):
    """
    Возвращает первый объект из набора, возвращаемого ф-цией
    ```objects_from_respoonse```

    Упрощенный вариант, когда в ответе от сервиса ожидаем
    гарантированно один объект
    :param response:
    :return:
    """
    try:
        return list(objects_from_response(response).values())[0]
    except IndexError:
        return None


def get_response_objects_map(response):
    """
    Возвращает словарь, ключом которого является класс объекта, а значением
    XML-селектор из объекта ответа ```laximo.Response```
    :param response:
    :return:
    """
    metas = [getattr(q, 'Meta') for q in response.request.queryset]

    def mapper(meta):
        selector = getattr(meta, 'selector', None)
        object_cls = getattr(meta, 'object_cls', None)
        return object_cls, selector

    objects_map = dict(list(map(mapper, metas)))
    return objects_map


def get_query_object(query_cls):
    """
    Возвращает ```object_cls```, который закреплен за данным ```query_cls```
    :param query_cls:
    :return:
    """
    # todo tests
    query_meta = getattr(query_cls, 'Meta')
    object_cls = getattr(query_meta, 'object_cls')
    return object_cls
