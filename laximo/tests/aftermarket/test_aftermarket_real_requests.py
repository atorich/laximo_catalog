# coding: utf-8

from unittest import TestCase

from laximo.aftermarket import objects, query
from laximo.objects import objects_from_response, object_from_response
from laximo.request import AftermarketRequest


class AMRequestTestCase(TestCase):
    object_cls = None
    query_cls = None
    query_params = None

    def setUp(self):
        super(AMRequestTestCase, self).setUp()
        q = self.query_cls(**self.query_params)
        self.request = AftermarketRequest()
        self.request.queryset.add_query(q)


class FindOEMTestCase(AMRequestTestCase):
    query_cls = query.FindOEM
    object_cls = objects.FindOEM
    query_params = {'oem': 'c110', 'options': 'crosses'}
    # query_params = {'oem': '078100105NX', 'options': 'crosses'}

    def setUp(self):
        super(FindOEMTestCase, self).setUp()
        self.response = self.request.call()

    def test_response_is_not_fault(self):
        self.assertFalse(self.response.is_fault)

    def test_create_objects_from_response(self):
        objs = list(objects_from_response(self.response).values())

        for obj in objs:
            self.assertIsInstance(obj, self.object_cls)

    def test_response_object_has_items(self):
        obj = object_from_response(self.response)
        self.assertGreater(len(obj.items), 0)

    def test_response_object_has_detail(self):
        obj = object_from_response(self.response)
        self.assertIsInstance(obj.items[0], objects.Detail)

    def test_response_object_detail_has_replacements(self):
        obj = object_from_response(self.response)
        detail = obj.items[0]
        replacements = detail.replacements
        self.assertGreater(len(replacements), 0)

    def test_response_object_detail_replacement_has_detail(self):
        obj = object_from_response(self.response)
        detail = obj.items[0]
        replacement = detail.replacements[0]
        self.assertIsInstance(replacement.detail, objects.Detail)

    def test_pure_real_case(self):
        """
        Реальный кейс, на котором что-то работало некорректно у Михаила
        """
        q = query.FindOEM(**{'oem': 'c110', 'options': 'crosses'})
        req = AftermarketRequest()
        req.queryset.add_query(q)
        resp = req.call()
        objs = list(objects_from_response(resp).values())
        for o in objs:
            for detail in o.items:
                for rep in detail.replacements:
                    rep_detail = rep.detail
                    self.assertIsInstance(rep_detail, objects.Detail)
                    break
                break
            break


class FindReplacementsTestCase(AMRequestTestCase):
    query_cls = query.FindReplacements
    object_cls = objects.FindReplacements
    query_params = {'detail_id': '17152535'}

    def setUp(self):
        super(FindReplacementsTestCase, self).setUp()
        self.response = self.request.call()

    def test_response_is_not_fault(self):
        self.assertFalse(self.response.is_fault)

    def test_create_objects_from_response(self):
        objs = list(objects_from_response(self.response).values())

        for obj in objs:
            self.assertIsInstance(obj, self.object_cls)

    def test_response_object_has_items(self):
        obj = object_from_response(self.response)
        self.assertGreater(len(obj.items), 0)

    def test_response_object_items_is_replacement_objects(self):
        obj = object_from_response(self.response)
        self.assertIsInstance(obj.items[0], objects.Replacement)


class FindDetailsTestCase(AMRequestTestCase):
    query_cls = query.FindDetails
    object_cls = objects.FindDetails
    query_params = {
        'detail_id': '13082407', 'options': query.SEARCH_OPTION_CROSSES
    }

    def setUp(self):
        super(FindDetailsTestCase, self).setUp()
        self.response = self.request.call()

    def test_response_is_not_fault(self):
        self.assertFalse(self.response.is_fault)

    def test_create_objects_from_response(self):
        objs = list(objects_from_response(self.response).values())

        for obj in objs:
            self.assertIsInstance(obj, self.object_cls)

    def test_response_object_has_detail(self):
        obj = object_from_response(self.response)
        self.assertIsInstance(obj.items[0], objects.Detail)


class FindOEMCorrectionTestCase(AMRequestTestCase):
    query_cls = query.FindOEMCorrection
    object_cls = objects.OEMCorrection
    query_params = {
        'oem': '07810010',
    }

    def setUp(self):
        super(FindOEMCorrectionTestCase, self).setUp()
        self.response = self.request.call()

    def test_response_is_not_fault(self):
        self.assertFalse(self.response.is_fault)

    def test_create_objects_from_response(self):
        objs = list(objects_from_response(self.response).values())

        for obj in objs:
            self.assertIsInstance(obj, self.object_cls)

    def test_response_items_is_detail(self):
        objs = list(objects_from_response(self.response).values())
        self.assertIsInstance(objs[0].items[0], objects.Detail)


class ListManufacturerTestCase(AMRequestTestCase):
    query_cls = query.ListManufacturer
    object_cls = objects.ListManufacturer
    query_params = {}

    def setUp(self):
        super(ListManufacturerTestCase, self).setUp()
        self.response = self.request.call()

    def test_response_is_not_fault(self):
        self.assertFalse(self.response.is_fault)

    def test_create_objects_from_response(self):
        objs = list(objects_from_response(self.response).values())

        for obj in objs:
            self.assertIsInstance(obj, self.object_cls)

    def test_response_items_is_manufacturer_info(self):
        objs = list(objects_from_response(self.response).values())
        self.assertIsInstance(objs[0].items[0], objects.ManufacturerInfo)


class ManufacturerInfoTestCase(AMRequestTestCase):
    query_cls = query.ManufacturerInfo
    object_cls = objects.ManufacturerInfo
    query_params = {
        'manufacturer_id': '4096'
    }

    def setUp(self):
        super(ManufacturerInfoTestCase, self).setUp()
        self.response = self.request.call()

    def test_response_is_not_fault(self):
        self.assertFalse(self.response.is_fault)

    def test_create_objects_from_response(self):
        objs = list(objects_from_response(self.response).values())

        for obj in objs:
            self.assertIsInstance(obj, self.object_cls)
