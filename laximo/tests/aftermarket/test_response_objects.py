# coding=utf-8

"""
Тесты на корректность создания объектов из XML-данных сервиса
Тестовые данные в формате XML расположены в каталоге ```TEST_DATA_DIRECTORY```
"""

import os

import laximo.aftermarket.objects
from laximo.tests.test_response_objects import BaseResponseObjectTestCase


class ResponseObjectTestCase(BaseResponseObjectTestCase):
    # object namespace required in objects models
    DEFAULT_NS = "http://Aftermarket.Kito.ec"

    # каталог, в котором расположены файлы с тестовыми данными
    TEST_DATA_DIRECTORY = '%s/./responses/objects' % os.path.dirname(__file__)


class FindReplacementsTestCase(ResponseObjectTestCase):
    filename = 'FindReplacements.xml'
    object_cls = laximo.aftermarket.objects.FindReplacements

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_response_object_has_items(self):
        self.assertGreater(len(self.object.items), 0)

    def test_response_object_items_is_replacement_objects(self):
        self.assertIsInstance(self.object.items[0], laximo.aftermarket.objects.Replacement)
