# coding=utf-8


from collections import OrderedDict
from unittest import TestCase

from laximo.aftermarket import query as am_query
from laximo.query import Query


class QueryTestCase(TestCase):
    class TestQuery(Query):
        name = 'test_func'

    def test_query_with_params(self):
        """
        Тестирует, что объект для запроса в API Laximo собирается коррекетно
        """
        params = OrderedDict([('param1', 'value1'), ('param2', 'value2')])
        q = self.TestQuery(params)
        self.assertEquals(str(q), 'test_func:param1=value1|param2=value2')


class SearchOptionTestCase(TestCase):
    def test_combine_two_single_search_options(self):
        crosses = am_query.SEARCH_OPTION_CROSSES
        images = am_query.SEARCH_OPTION_IMAGES
        combined = crosses | images
        self.assertEquals(combined, 'crosses,images')

    def test_combine_single_search_option_with_combined(self):
        crosses = am_query.SEARCH_OPTION_CROSSES
        images = am_query.SEARCH_OPTION_IMAGES
        names = am_query.SEARCH_OPTION_NAMES
        combined = crosses | images
        combined = combined | names
        self.assertEquals(combined, 'crosses,images,names')
