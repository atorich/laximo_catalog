# coding=utf-8

from unittest import TestCase

import mock

from laximo.request import RequestTransport


class LaximoRequestTransportTestCase(TestCase):
    def setUp(self):
        self.transport = RequestTransport()
        self.mock()

    def tearDown(self):
        self.unmock()

    def mock(self):
        self.send_patch = mock.patch(
            'requests.Session.send', new=mock.MagicMock())
        self.send_patch.start()

    def unmock(self):
        self.send_patch.stop()

    def test_request_has_nedeed_headers(self):
        """Тестирует, что запрос содержит необходимые заголовки"""
        request = self.transport.create_request()
        headers = request.headers
        self.assertIn('SOAPAction', headers)
        self.assertIn('User-Agent', headers)
        self.assertIn('Accept', headers)
        self.assertIn('Content-Type', headers)

    def test_transport_default_http_params(self):
        """Тестирует, что в запрос прилетают правильные параметры по умолчанию
        из конфигурации"""
        self.transport.send(mock.MagicMock(url='http://testtest'))
        send_args = self.send_patch.target.send.call_args
        _, params = send_args
