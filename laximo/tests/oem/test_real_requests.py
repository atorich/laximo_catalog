# coding=utf-8
"""
Тестирует непосредственно ответы на запросы от веб-сервиса. По большому счету
эти тесты необходимы для того, чтобы вовремя эффективно отслеживать изменения
в формате данных на той стороне. В принципе, при сборке/деплое каждый раз их
можно не гонять
"""


from unittest import TestCase

import laximo.oem.objects
from laximo import objects
from laximo.oem import query as oem_query
from laximo.request import OEMRequest


def dump_response_data(inst, response):
    name = inst.__class__.__name__.replace('TestCase', '')
    with open('%s.xml' % name, 'w') as f:
        print(response.data, file=f)


class OEMRequestTestCase(TestCase):
    object_cls = None
    query_cls = None
    query_params = None

    def setUp(self):
        super(OEMRequestTestCase, self).setUp()
        query = self.query_cls(**self.query_params)
        self.request = OEMRequest()
        self.request.queryset.add_query(query)


class ResponseMixin(object):
    def setUp(self):
        super(ResponseMixin, self).setUp()
        self.response = self.request.call()

    def test_response_is_not_fault(self):
        """
        Тестирует все унаследованные кейсы на то, что запрос, посланный
        с указанными в ```query_params``` параметрами возвращает данные, а не
        ошибоку (Fault)
        """
        self.assertFalse(self.response.is_fault)

    def test_create_objects_from_response(self):
        objs = list(objects.objects_from_response(self.response).values())

        for obj in objs:
            self.assertIsInstance(obj, self.object_cls)


class GetCatalogInfoObjectTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.GetCatalogInfo
    object_cls = laximo.oem.objects.CatalogInfo
    query_params = {'catalog': 'AP1211'}


class ListCatalogsObjectTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.ListCatalogs
    object_cls = laximo.oem.objects.CatalogList
    query_params = {'catalog': 'AP1211'}


class FindVehicleByVINTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.FindVehicleByVIN
    object_cls = laximo.oem.objects.FindVehicleByVIN
    query_params = {
        'vin': 'WAUBH54B11N111054'
    }


class FindVehicleByFrameTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.FindVehicleByFrame
    object_cls = laximo.oem.objects.FindVehicleByFrame
    query_params = {
        'catalog': 'TA201511', 'frame': 'XZU423', 'frameno': '0001026'
    }


class FindVehicleByFrameWithoutCatalog(FindVehicleByFrameTestCase):
    query_params = {
        'frame': 'FNN15',
        'frameno': '502358',
    }


class FindVehicleByWizardTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.FindVehicleByWizard
    object_cls = laximo.oem.objects.FindVehicleByWizard
    query_params = {
        'catalog': 'AU1108', 'ssd': '$WiVITAF0AQRNB3ZaVU5MBnQACgJKACtXTAEA$'
    }


class GetVehicleInfoTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.GetVehicleInfo
    object_cls = laximo.oem.objects.Vehicle
    query_params = {
        'catalog': 'TA201511', 'vehicle_id': '206343',
        'ssd': '$HQwdcgEEAwEHB0gdDBdya25gBgEHG1tVSQMDBgNMbHRDEQczCmlFWVYCUxc$'
    }


class GetWizardTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.GetWizard
    object_cls = laximo.oem.objects.Wizard
    query_params = {
        'catalog': 'RENAULT201507',
        'ssd': '$WiVIAQ$',
    }


class ExecCustomOperationTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.ExecCustomOperation
    object_cls = laximo.oem.objects.ExecCustomOperation
    query_params = {
        'catalog': 'NIS201504',
        'operation': 'searchByModelCode',
        'modelCode': 'BATARJAG10EDA-----'
    }


class GetUnitInfoTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.GetUnitInfo
    object_cls = laximo.oem.objects.Unit
    query_params = {
        'catalog': 'CFiat112013', 'unit_id': '40697',
        'ssd': '$GAILQmxXWlZzBEgSABhAcwYFAgpKQAoXEFNdQFByAkpADg0QUV9RalxcdAZcVEsKBgEMTEFRQAIWEF1dUXUCTUdRGDoaXVd0BUxHVVQHADpdV1F1AwMEBltKSQJNWUNBcwUDW1RMBwMEBAIDBEgPBABtXFJqX1xaQgMlSwtNV1RGbFdZDyVKYE1XWl5cQUQrUUkATVdZW1BcUytVB1ddU1xcVmtYBCVIBgsCBAUDSlsECw1acwUHTFBcVxgWEEFsWlpyaXJ3WFRLAgMEBQIHAQdZU0tMRlpcRnpQdl9VTwsESlxWcwVIDxcQRFZ0ZkxsdGQtJjgDSmJMREsMFxUCC0JsV1pWcwUGWw$',
    }


class ListUnitsTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.ListUnits
    object_cls = laximo.oem.objects.UnitList
    query_params = {
        'catalog': 'CFiat112013', 'vehicle_id': '143',
        'ssd': '$CAQNbVBbUXIAZkgIChVdRkZ1BgMGSA4LHltdUWpcXHQHX11PAwQES19cWkIDJUgATU1QU0F0BFtVQExQXFRBQF1FNAsWcmlydAECBgZbVUkCBwEEAAUGSB0AEVtQWFBtXVt2W1JIAAsBAAtNWUAYJUgHAwUABwMEB1tVSUxaUHUDTVBEAhMccmBKUVNKa1kNOhRdXUBdcgENSDQlVERlcGdYRGVdJgpY$',
        'category_id': '-1',
    }


class ListCategoriesTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.ListCategories
    object_cls = laximo.oem.objects.CategoryList
    query_params = {
        'catalog': 'CFiat112013', 'vehicle_id': '143',
        'category_id': '-1',
    }


class GetFilterByUnitTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.GetFilterByUnit
    object_cls = laximo.oem.objects.UnitFilters
    query_params = {
        'catalog': 'RENAULT201507',
        'filter': 'JENBUEAxMjgwQNCj0LLQtdC70LjRh9C10L3QvdCw0Y8g0L-QvtC70LXQt9C'
                  '90LDRjyDQvdCw0LPRgNGD0LfQutCwI9Ch0YLQsNC90LTQsNGA0YLQvdCw0Y'
                  '8g0L-QvtC70LXQt9C90LDRjyDQvdCw0LPRgNGD0LfQutCwI9Ch0YLQsNC90'
                  'LTQsNGA0YLQvdCw0Y8g0L-QvtC70LXQt9C90LDRjyDQvdCw0LPRgNGD0LfQ'
                  'utCwICs_',
        'vehicle_id': '1280',
        'unit_id': '6261',
        'ssd': '$BQoJQHMFBwoDSkYKFxxcR3R4AQBKQwIBOQZNXVhVcwQGW1NMAgoAS1xXTHZbG'
               'xddQ0ZnXVxAdlpXQQJNWVBcRldZDwA5fwAHBgFxSlsOCwxRXFBQYFxbQisoSgF'
               'NQlxcc2JwWi46An53cwAEDARcU00LTUFWXVdRdiVWSgYDBQBMbHRFBQwfSAQED'
               'UBHdRcVBgtyAg$',
    }

    def test_filters_instantiate_ok(self):
        obj = objects.object_from_response(self.response)
        for item in obj.items:
            self.assertIsInstance(item, laximo.oem.objects.Filter)


class ListDetailByUnitTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.ListDetailByUnit
    object_cls = laximo.oem.objects.DetailList
    query_params = {
        'catalog': 'CFiat112013',
        'unit_id': '40697',
        'ssd': '$GAILQmxXWlZzBEgSABhAcwYFAgpKQAoXEFNdQFByAkpADg0QUV9RalxcdAZcVEsKBgEMTEFRQAIWEF1dUXUCTUdRGDoaXVd0BUxHVVQHADpdV1F1AwMEBltKSQJNWUNBcwUDW1RMBwMEBAIDBEgPBABtXFJqX1xaQgMlSwtNV1RGbFdZDyVKYE1XWl5cQUQrUUkATVdZW1BcUytVB1ddU1xcVmtYBCVIBgsCBAUDSlsECw1acwUHTFBcVxgWEEFsWlpyaXJ3WFRLAgMEBQIHAQdZU0tMRlpcRnpQdl9VTwsESlxWcwVIDxcQRFZ0ZkxsdGQtJjgDSmJMREsMFxUCC0JsV1pWcwUGWw$',
    }


class ListImageMapByUnitTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.ListImageMapByUnit
    object_cls = laximo.oem.objects.ImageMapList
    query_params = {
        'catalog': 'EAU1007',
        'unit_id': '1890287',
        'ssd': '$HToUW1d0BAcHDEgdOhJbV3QGAQQESB06DVtXdAMLBg1INCUfVEoNdAJpXVk9V'
               'Vg$'
    }


class GetFilterByDetailTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.GetFilterByDetail
    object_cls = laximo.oem.objects.DetailFilters
    query_params = {
        'catalog': 'RENAULT201507',
        'filter': 'JENBUEAxMjgwQNCj0LLQtdC70LjRh9C10L3QvdCw0Y8g0L-QvtC70LXQt9C'
                  '90LDRjyDQvdCw0LPRgNGD0LfQutCwI9Ch0YLQsNC90LTQsNGA0YLQvdCw0Y'
                  '8g0L-QvtC70LXQt9C90LDRjyDQvdCw0LPRgNGD0LfQutCwICs_',
        'vehicle_id': '1280',
        'unit_id': '6256',
        'detail_id': '0',
        'ssd': '$BQoJQHMFBwoDSkYKFxxcR3R4AQBKQwIBOQFNXVhVcwQGWlRBCgcGS1xXTHZbG'
               'xddQ0ZnXVxAdlpXQQJNWVBcRldZDwA5fwAHBgFySlsOCwxRXFBQYFxbQisoSgF'
               'NQlxcc2JwWi46An53cwAEDARcU00LTUFWXVdRdiVWSgIBBQVMUEZ2WhsmcmB3f'
               'UtnRl4gEUprEg$'
    }

    def test_filters_instantiate_ok(self):
        obj = objects.object_from_response(self.response)
        for item in obj.items:
            self.assertIsInstance(item, laximo.oem.objects.Filter)


class ListQuickGroupTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.ListQuickGroup
    object_cls = laximo.oem.objects.QuickGroupTree
    query_params = {
        'catalog': 'MAZDA201510',
        'vehicle_id': '1421500770',
        'ssd': '$BQEBcgJKQ1tddHwmPz56AgZzBQMFAl9dTgcFSlNAc1JINCUzamZMRGprTVxTP'
               'Fg$'
    }


class ListQuickDetailTestCase(ResponseMixin, OEMRequestTestCase):
    query_cls = oem_query.ListQuickDetail
    object_cls = laximo.oem.objects.QuickDetailTree
    query_params = {
        'catalog': 'MAZDA201510',
        'vehicle_id': '1421500770',
        'ssd': '$BQEBcgJKQ1tddHwmPz56AgZzBQMFAl9dTgcFSlNAc1JINCUzamZMRGprTVxTP'
               'Fg$',
        'quick_group_id': '10359',
        'all': '1',
    }


class GetCatalogInfoWithWizardTestCase(TestCase):
    def setUp(self):
        self.request = OEMRequest()
        super(GetCatalogInfoWithWizardTestCase, self).setUp()

        self.request.queryset.add_query(
            oem_query.GetCatalogInfo(catalog='TA201511')
        )

        self.request.queryset.add_query(
            oem_query.GetWizard(catalog='TA201511')
        )

    def test_response_is_not_fault(self):
        response = self.request.call()
        self.assertFalse(response.is_fault)

    def test_create_objects_from_response(self):
        response = self.request.call()
        objs = objects.objects_from_response(response)
        self.assertEqual(len(objs), 2)
        self.assertNotIn(None, objs)

    def test_objects_types_ok(self):
        response = self.request.call()
        objs = list(objects.objects_from_response(response).values())
        objs_cls = [type(o) for o in objs]

        self.assertIn(laximo.oem.objects.CatalogInfo, objs_cls)
        self.assertIn(laximo.oem.objects.Wizard, objs_cls)
