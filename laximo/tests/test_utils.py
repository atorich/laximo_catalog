# coding=utf-8

from unittest import TestCase

from laximo import utils


class SlugifyTestCase(TestCase):
    def test_slugify_with_spaces_and_slashes(self):
        value = 'name / subname'
        slug = utils.slugify(value)
        self.assertEquals('name-subname', slug)
