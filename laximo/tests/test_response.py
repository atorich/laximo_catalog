# coding=utf-8


from unittest import TestCase

import mock
from suds.sax.parser import Parser
from suds.transport import Reply

from laximo.exceptions import MalformedResponse
from laximo.request import BaseRequest, OEMRequest
from laximo.response import Fault


class FakeMixin(object):
    """
    Тестирует ответ с ошибкой (узел Fault)
    """

    class FakeRequest(BaseRequest):
        # suds резолвит wsdl в конструкторе, поэтому даже если
        # мы собираемся замокать вызов метода сервиса, в урле
        # все равно должно быть что-то валидное.

        # Вообще так делать нельзя, то есть это todo
        # но если очень хочется (срочно), то можно

        wsdl = OEMRequest.wsdl

        class Response(object):
            pass

    def setUp(self):
        self.parser = Parser()
        self.fake_request = self.FakeRequest()
        super(FakeMixin, self).setUp()

    def mock(self, return_value):
        self.send_patch = mock.patch(
            'laximo.request.RequestTransport.send',
            new=mock.MagicMock(return_value=return_value)
        )
        self.send_patch.start()

    def unmock(self):
        self.send_patch.stop()


class FaultResponseTestCase(FakeMixin, TestCase):

    def test_invalidparameter_fault(self):
        xml = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
        <SOAP-ENV:Header xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"/>
        <soapenv:Body>
            <soapenv:Fault><faultcode>soapenv:Server</faultcode>
                <faultstring>E_INVALIDPARAMETER:PARAMPARAM</faultstring>
                <detail><ns1:Exception xmlns:ns1="http://WebCatalog.Kito.ec"/></detail>
            </soapenv:Fault>
        </soapenv:Body>
        </soapenv:Envelope>
        """
        reply = Reply(500, {}, xml)

        self.mock(return_value=reply)
        response = self.fake_request.call()
        self.unmock()

        self.assertTrue(response.is_fault)
        self.assertIsInstance(response.fault, Fault)
        self.assertEquals(response.fault.code, 'E_INVALIDPARAMETER')
        self.assertGreater(len(response.fault.message), 0)


class MalformedResponseError(FakeMixin, TestCase):
    def test_502_nginx_response_proper_exception_raises(self):
        xml = """<html>
        <head><title>502 Bad Gateway</title></head>
        <body bgcolor="white">
        <center><h1>502 Bad Gateway</h1></center>
        <hr><center>nginx/1.8.1</center>
        </body>
        </html>
        """
        reply = Reply(200, {}, xml)

        self.mock(return_value=reply)
        with self.assertRaises(MalformedResponse):
            self.fake_request.call()
        self.unmock()

    def test_empty_response_proper_exception_raises(self):
        xml = ""
        reply = Reply(200, {}, xml)

        self.mock(return_value=reply)
        with self.assertRaises(MalformedResponse):
            self.fake_request.call()
        self.unmock()
