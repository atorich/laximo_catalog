# coding=utf-8

"""
Тесты на корректность создания объектов из XML-данных сервиса
Тестовые данные в формате XML расположены в каталоге ```TEST_DATA_DIRECTORY```
"""

import os


from unittest import TestCase

from lxml import etree

import laximo.oem.objects
from laximo import objects


class BaseResponseObjectTestCase(TestCase):
    """
    Базовый класс для тест кейса создания объекта из XML данных

    Т.к. используется не XML-ответ целиком, а его часть, пространство имен,
    определяемое в ответе в него не попадает.
    В процессе разбора дерева на поддеревья необходимый namespace проставляется
    средствами lxml, а в данном случае, т.к. никаких выборок не производится,
    необходимо добавлять его туда вручную (он необходим для корректной
    работы моделей (dexml))

    Поэтому мы в

    Attributes:
        object_cls  Класс объекта, который должен быть создан
        filename    Имя XML файла с данными
    """
    object_cls = None
    filename = None
    ns = None

    # object namespace required in objects models
    DEFAULT_NS = "http://WebCatalog.Kito.ec"

    # каталог, в котором расположены файлы с тестовыми данными
    TEST_DATA_DIRECTORY = '%s/./responses/objects' % os.path.dirname(__file__)

    def setUp(self):
        self.object = self.create_object()

    def create_object(self):
        """
        Создает объект класса ```object_cls``` из загруженных данных
        :return:
        """
        tree = self.load_data()
        data = self.patch_namespace(tree)
        data = self.process_data(data)
        return objects.object_from_xml_element(self.object_cls, data)

    def load_data(self):
        """
        Загружает тестовые данные
        :return:
        """
        filename = '{directory}/{name}'.format(
            directory=self.TEST_DATA_DIRECTORY,
            name=self.filename
        )
        return etree.parse(filename)

    def patch_namespace(self, tree):
        """
        Добавляет пространство имен в сырые тестовые данные
        :param tree:
        :return:
        """
        ns = self.ns or self.DEFAULT_NS
        root = tree.getroot()
        root.set('xmlns', ns)
        return root

    def process_data(self, data):
        return data


class CatalogInfoTestCase(BaseResponseObjectTestCase):
    """
    Тестирует, что объект CatalogInfo создается корректно и содержит все
    необходимые атрибуты

    NB! Кейс с отсутствующим extensions покрывается в тесте ListCatalogs т.к.
    в наборе данных присутствуют и такие варианты
    """
    filename = 'CatalogInfo.xml'
    object_cls = laximo.oem.objects.CatalogInfo

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_catalog_contains_features(self):
        self.assertEquals(len(self.object.features), 2)

    def test_catalog_features_instantiate_ok(self):
        for feature in self.object.features:
            self.assertIsInstance(feature, laximo.oem.objects.CatalogFeature)

    def test_catalog_contains_extensions(self):
        self.assertTrue(hasattr(self.object, 'extensions'))
        self.assertTrue(hasattr(self.object.extensions, 'operations'))

    def test_catalog_extensions_contains_operations(self):
        self.assertEquals(len(self.object.extensions.operations), 1)

    def test_catalog_operation_values(self):
        operation = self.object.extensions.operations[0]
        self.assertEquals(operation.kind, "search_vehicle")
        self.assertEquals(operation.name, "searchByChassis")

    def test_catalog_operation_contains_fields(self):
        operation = self.object.extensions.operations[0]
        self.assertEquals(len(operation.fields), 1)

    def test_catalog_operation_field_values(self):
        operation = self.object.extensions.operations[0]
        field = operation.fields[0]
        self.assertEquals(field.name, "chassis")
        self.assertEquals(field.pattern, "[\w\d]{6,8}")


class ListCatalogsTestCase(BaseResponseObjectTestCase):
    filename = 'ListCatalogs.xml'
    object_cls = laximo.oem.objects.CatalogList

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_items_instantiate_ok(self):
        self.assertEquals(len(self.object.items), 48)

        for item in self.object.items:
            self.assertIsInstance(item, laximo.oem.objects.CatalogInfo)


class FilterMixin(object):
    filename = 'Filter.xml'
    object_cls = laximo.oem.objects.Filter


class FilterTestCase(FilterMixin, BaseResponseObjectTestCase):
    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_filter_has_values(self):
        values = self.object.values
        self.assertEquals(len(values), 3)

    def test_values_instantiate_ok(self):
        for value in self.object.values:
            self.assertIsInstance(value, laximo.oem.objects.FilterValue)


class FilterWithoutValuesTestCase(FilterMixin, BaseResponseObjectTestCase):
    def process_data(self, data):
        for values in data.xpath("//values"):
            values.getparent().remove(values)
        return data

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_filter_values_is_none(self):
        self.assertEquals(self.object.values, [])


class VehicleTestCase(BaseResponseObjectTestCase):
    filename = 'Vehicle.xml'
    object_cls = laximo.oem.objects.Vehicle

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_model_attribute_ok(self):
        self.assertEquals(self.object.model, "URJ202L-GNTVKW")

    def test_undefined_attribute_ok(self):
        with self.assertRaises(AttributeError):
            value = self.object.undefined_attr


class WizardTestCase(BaseResponseObjectTestCase):
    filename = 'Wizard.xml'
    object_cls = laximo.oem.objects.Wizard

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_parameters_in_obj(self):
        parameters = self.object.parameters
        self.assertEquals(len(parameters), 23)

    def test_wizard_parameter_contains_options(self):
        parameter = self.object.parameters[0]
        self.assertTrue(hasattr(parameter, 'options'))

    def test_wizard_options_is_populated_when_exists(self):
        parameter = filter(
            lambda p: p.name == 'Frame', self.object.parameters
        )[0]
        self.assertEquals(len(parameter.options), 14)


class UnitTestCase(BaseResponseObjectTestCase):
    filename = 'UnitInfo.xml'
    object_cls = laximo.oem.objects.Unit

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_unit_has_note(self):
        self.assertEquals(self.object.note, '(1501- )2AR#')


class UnitWithNoteAsRowAttribute(BaseResponseObjectTestCase):
    filename = 'UnitInfo.xml'
    object_cls = laximo.oem.objects.Unit

    def process_data(self, data):
        for values in data.xpath("//attribute"):
            values.getparent().remove(values)

        row = data.xpath('//row')[0]
        row.attrib['note'] = '(1501- )2AR#'
        return data

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_unit_has_note(self):
        self.assertEquals(self.object.note, '(1501- )2AR#')


class UnitWithoutNote(BaseResponseObjectTestCase):
    filename = 'UnitInfo.xml'
    object_cls = laximo.oem.objects.Unit

    def process_data(self, data):
        for values in data.xpath("//attribute"):
            values.getparent().remove(values)
        return data

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_unit_has_note(self):
        self.assertEquals(self.object.note, '')


class UnitListTestCase(BaseResponseObjectTestCase):
    filename = 'ListUnits.xml'
    object_cls = laximo.oem.objects.UnitList

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_items_instantiate_ok(self):
        self.assertEquals(len(self.object.items), 4)

        for item in self.object.items:
            self.assertIsInstance(item, laximo.oem.objects.Unit)


class CategoryTestCase(BaseResponseObjectTestCase):
    filename = 'Category.xml'
    object_cls = laximo.oem.objects.Category

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)


class CategoryListTestCase(BaseResponseObjectTestCase):
    filename = 'ListCategories.xml'
    object_cls = laximo.oem.objects.CategoryList

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_items_instantiate_ok(self):
        self.assertEquals(len(self.object.items), 5)

        for item in self.object.items:
            self.assertIsInstance(item, laximo.oem.objects.Category)


class DetailTestCase(BaseResponseObjectTestCase):
    filename = 'Detail.xml'
    object_cls = laximo.oem.objects.Detail

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_detail_has_note(self):
        self.assertEquals(self.object.note, u'ПРИМЕЧАНИЕ')

    def test_detail_has_amount(self):
        self.assertEquals(self.object.amount, u'1')

    def test_detail_has_partspec(self):
        self.assertEquals(self.object.attributes['partspec'].value, u'OS=0.20')


class DetailListTestCase(BaseResponseObjectTestCase):
    filename = 'DetailList.xml'
    object_cls = laximo.oem.objects.DetailList

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_items_instantiate_ok(self):
        self.assertEquals(len(self.object.items), 3)

        for item in self.object.items:
            self.assertIsInstance(item, laximo.oem.objects.Detail)


class ImageMapTestCase(BaseResponseObjectTestCase):
    filename = 'ImageMap.xml'
    object_cls = laximo.oem.objects.ImageMap

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)


class ImageMapListTestCase(BaseResponseObjectTestCase):
    filename = 'ImageMapList.xml'
    object_cls = laximo.oem.objects.ImageMapList

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_items_instantiate_ok(self):
        self.assertEquals(len(self.object.items), 3)

        items = sorted(self.object.items, key=lambda o: o.code)

        for i, item in enumerate(items):
            self.assertIsInstance(item, laximo.oem.objects.ImageMap)
            self.assertEquals(item.code, str(i + 1))
            self.assertTrue(hasattr(item, 'x1'))
            self.assertTrue(hasattr(item, 'y1'))
            self.assertTrue(hasattr(item, 'x2'))
            self.assertTrue(hasattr(item, 'y2'))


class QuickGroupTestCase(BaseResponseObjectTestCase):
    filename = 'QuickGroup.xml'
    object_cls = laximo.oem.objects.QuickGroup

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_object_items_tree(self):
        self.assertEquals(len(self.object.items), 1)
        self.assertEquals(self.object.quickgroupid, "10101")

        self.assertEquals(len(self.object.items[0].items), 1)
        self.assertEquals(self.object.items[0].quickgroupid, "11790")

        self.assertEquals(len(self.object.items[0].items[0].items), 1)
        self.assertEquals(self.object.items[0].items[0].quickgroupid, "11800")

        self.assertEquals(len(self.object.items[0].items[0].items), 1)
        self.assertEquals(
            self.object.items[0].items[0].items[0].quickgroupid, "11803"
        )


class QuickGroupListTreeCase(BaseResponseObjectTestCase):
    filename = 'QuickGroupList.xml'
    object_cls = laximo.oem.objects.QuickGroupTree

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)
        self.assertIsInstance(self.object.items[0],
                              laximo.oem.objects.QuickGroup)

    def test_items_instantiate_ok(self):
        self.assertEquals(len(self.object.items), 1)
        self.assertEquals(len(self.object.items[0].items), 3)

        for item in self.object.items:
            self.assertIsInstance(item, laximo.oem.objects.QuickGroup)


class QuickDetailList(BaseResponseObjectTestCase):
    filename = 'QuickDetailList.xml'
    object_cls = laximo.oem.objects.QuickDetailTree

    def test_object_instantiate_ok(self):
        self.assertIsNotNone(self.object)
        self.assertIsInstance(self.object, self.object_cls)

    def test_categories_is_the_root(self):
        self.assertTrue(hasattr(self.object, 'categories'))
        self.assertIsInstance(self.object.categories[0],
                              laximo.oem.objects.Category)
        self.assertIsInstance(
            self.object.categories[0].units[0], laximo.oem.objects.Unit
        )
        self.assertIsInstance(
            self.object.categories[0].units[0].details[0],
            laximo.oem.objects.Detail
        )
