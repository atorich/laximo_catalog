
import logging

# fix https://github.com/nose-devs/nose/issues/795
logging.getLogger('suds.mx.core').setLevel(logging.ERROR)
logging.getLogger('suds.mx.literal').setLevel(logging.ERROR)
logging.getLogger('suds.wsdl').setLevel(logging.ERROR)
logging.getLogger('suds.xsd.schema').setLevel(logging.ERROR)
logging.getLogger('suds.xsd.query').setLevel(logging.ERROR)
logging.getLogger('suds.xsd.sxbase').setLevel(logging.ERROR)
logging.getLogger('suds.xsd.sxbasic').setLevel(logging.ERROR)
logging.getLogger('suds.metrics').setLevel(logging.ERROR)
logging.getLogger('suds.resolver').setLevel(logging.ERROR)
logging.getLogger('suds.client').setLevel(logging.ERROR)
