# coding: utf-8



from collections import OrderedDict

from dexml import Model, fields
from funcy import cached_property

from laximo.objects import IterableMixin, LabeledString
from laximo.utils import slugify


class Attribute(Model):
    """
    Описывает атрибут, оформленный в виде отдельного тега
    см. Unit, Detail
    """

    class meta(object):
        tagname = 'attribute'
        namespace = 'http://WebCatalog.Kito.ec'

    key = fields.String()
    value = fields.String()
    name = fields.String(default='')


class CatalogFeature(Model):
    """
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListCatalogs
    ```//ListCatalogs/row/features/feature```

    Attributes:
        name - Наименование
        example - Пример
    """

    class meta(object):
        tagname = 'feature'
        namespace = 'http://WebCatalog.Kito.ec'

    name = fields.String()
    example = fields.String(default='')


class CatalogOperationField(Model):
    """

    Attributes:
        name - Имя параметра
        pattern - Регулярное выражение, с помощью которого возможно проверить
            корректность ввода данных от пользователя
        description - Наименование операции на запрошенном языке
    """

    class meta(object):
        tagname = 'field'
        namespace = 'http://WebCatalog.Kito.ec'

    name = fields.String()
    pattern = fields.String()
    description = fields.String()


class CatalogOperation(Model):
    """
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListCatalogs
    ```//ListCatalogs/row/operations/operation```

    Attributes:
        name - Имя операции.
            Должно быть передано в функцию ExecCustomOperation
        kind - Тип операции.
            На текущий момент поддерживается только поиск автомобилей
            (search_vehicle)
        description - Наименование операции на запрошенном языке
    """

    class meta(object):
        tagname = 'operation'
        namespace = 'http://WebCatalog.Kito.ec'

    name = fields.String()
    kind = fields.String()
    description = fields.String()
    fields = fields.List('CatalogOperationField')


class CatalogExtensions(Model):
    class meta(object):
        namespace = 'http://WebCatalog.Kito.ec'
        tagname = 'extensions'

    operations = fields.List('CatalogOperation', tagname='operations')


class CatalogInfo(Model):
    """
    Информация о каталоге
    Описание данных:
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:GetCatalogInfo/en
    """

    class meta(object):
        tagname = 'row'
        namespace = 'http://WebCatalog.Kito.ec'

    code = fields.String()
    icon = fields.String()
    brand = fields.String()
    name = fields.String()

    # Не смотря на то, что поле version там помечено обязательным, иногда
    # оно все же не приходит, поэтому выставлен default=''
    version = fields.String(default='')

    features = fields.List('CatalogFeature', tagname='features')
    extensions = fields.Model('CatalogExtensions', required=False)

    # todo operations

    vinexample = fields.String()
    frameexample = fields.String()

    @property
    def brand_lc(self):
        return str(self.brand).lower()

    @cached_property
    def slug(self):
        return slugify(str(self.name))

    @cached_property
    def _available_features(self):
        # todo test it with is_smth_available
        return [f.name for f in self.features]

    @property
    def is_wizard_available(self):
        return 'wizardsearch2' in self._available_features

    @property
    def is_vinsearch_available(self):
        return 'vinsearch' in self._available_features

    @property
    def is_framesearch_available(self):
        return 'framesearch' in self._available_features

    @property
    def is_quickgroups_available(self):
        return 'quickgroups' in self._available_features


class CatalogList(Model, IterableMixin):
    """
    Список каталогов
    Описание данных:
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListCatalogs
    """

    class meta(object):
        tagname = 'ListCatalogs'
        namespace = 'http://WebCatalog.Kito.ec'

    items = fields.List('CatalogInfo')


class VehicleAttribute(Model):
    """
    Описывает атрибут автомобиля
    См. http://technologytrade.ru/index.php/Laximo_Web-services:OEM:GetVehicleInfo

    """

    class meta(object):
        tagname = 'attribute'
        namespace = 'http://WebCatalog.Kito.ec'

    key = fields.String()
    name = fields.String()
    value = fields.Value()


class Vehicle(Model):
    """
    Автомобиль
    Описание данных:
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:GetVehicleInfo
    """

    class meta(object):
        tagname = 'row'
        namespace = 'http://WebCatalog.Kito.ec'

    vehicleid = LabeledString()
    name = LabeledString(
        verbose_name=u'Наименование'
    )

    brand = LabeledString(
        default='',
        verbose_name=u'Бренд'
    )
    catalog = LabeledString(
        default='',
        verbose_name=u'Код каталога'
    )
    ssd = LabeledString(
        default='',
        verbose_name=u'SSD'
    )

    # <МамаРодиМеняОбратно>
    # Все эти свойства бывают завернуты в <attribute key="..." ... />,
    # а бывает и нет
    # Названы тоже от балды - присутствует lowercase, mixedCase, etc...
    model = LabeledString(
        default='',
        verbose_name=u'Модель'
    )
    grade = LabeledString(
        default='',
        verbose_name=u'Комплектация'
    )
    transmission = LabeledString(
        default='',
        verbose_name=u'Тип КП'
    )
    creationregion = LabeledString(
        default='',
        verbose_name=u'Код региона производства'
    )
    framecolor = LabeledString(
        default='',
        verbose_name=u'Цвет кузова'
    )
    trimcolor = LabeledString(
        default='',
        verbose_name=u'Цвет салона'
    )
    catprodperiod = LabeledString(
        default='',
        verbose_name=u'Период выпуска модели'
    )
    modprodperiod = LabeledString(
        default='',
        verbose_name=u'Период выпуска модификации',
    )
    doors = LabeledString(
        default='',
        verbose_name=u'Кол-во дверей',
    )

    date = LabeledString(
        default='',
        verbose_name=u'Дата производства'
    )
    manufactured = LabeledString(
        default='',
        verbose_name=u'Год прозиодства автомобиля'
    )
    datefrom = LabeledString(
        default='',
        verbose_name=u'Выпускается с'
    )
    dateto = LabeledString(
        default='',
        verbose_name=u'Выпускалась до'
    )
    frame = LabeledString(
        default='',
        verbose_name=u'Код кузова'
    )
    frames = LabeledString(
        default='',
        verbose_name=u'Коды кузова'
    )
    framefrom = LabeledString(
        default='',
        verbose_name=u'Диапазон кузовов'
    )
    frameto = LabeledString(
        default='',
        verbose_name=u'Диапазон кузововов'
    )
    engine = LabeledString(
        default='',
        verbose_name=u'Код двигателя'
    )
    engine1 = LabeledString(
        default='',
        verbose_name=u'Код двигателя'
    )
    engine2 = LabeledString(
        default='',
        verbose_name=u'Код двигателя'
    )
    engineno = LabeledString(
        default='',
        verbose_name=u'Диапазон номеров двигателя'
    )
    options = LabeledString(
        default='',
        verbose_name=u'Перечень опций'
    )
    modelyearfrom = LabeledString(
        default='',
        verbose_name=u'Год выпуска модели'
    )
    modelyearto = LabeledString(
        default='',
        verbose_name=u'Год выпуска модели'
    )
    modification = LabeledString(
        default='',
        verbose_name=u'Модификация'
    )
    description = LabeledString(
        default='',
        verbose_name=u'Описание'
    )
    market = LabeledString(
        default='',
        verbose_name=u'Регион поставки',
    )
    destinationregion = market
    # </МамаРодиМеняОбратно>

    attrs = fields.Dict('VehicleAttribute', key='key')

    visible_attr_names = [
        'brand',
        'name',
        'model',
        'options',
        'engine',
        'engineno',
        'engine1',
        'engine2',
        'frame',
        'frames',
        'transmission',
        'framecolor',
        'trimcolor',
        'doors',
        'description',

        'market',
        'destinationregion',

        'date',

        'catprodperiod',
        'modelyearfrom',
        'modelyearto',

        'modprodperiod',
        'datefrom',
        'dateto'
    ]

    @property
    def visible_attrs(self):
        cls = self.__class__
        attrs = OrderedDict()

        for name in self.visible_attr_names:
            if not hasattr(cls, name):
                continue

            if not hasattr(self, name):
                continue

            field = getattr(cls, name)
            value = getattr(self, name)

            if not value:
                continue

            attrs[field.field_name] = field.verbose_name

        return attrs

    @property
    def brand_lc(self):
        return str(self.brand).lower()

    @classmethod
    def parse(cls, xml):
        self = super(Vehicle, cls).parse(xml)

        for attr in self.attrs:
            # т.к. регистр в каталоге встречается от балды
            # (camelCase, lowercase, etc), приведем все к единому виду
            attr_lo = attr.lower()
            if not hasattr(self, attr_lo):
                continue
            setattr(self, attr_lo, self.attrs[attr].value)

        return self


class FindVehicleByWizard(Model):
    class meta(object):
        tagname = 'FindVehicleByWizard2'
        namespace = 'http://WebCatalog.Kito.ec'

    items = fields.List('Vehicle')


class FindVehicleByFrame(Model):
    class meta(object):
        tagname = 'FindVehicleByFrame'
        namespace = 'http://WebCatalog.Kito.ec'

    items = fields.List('Vehicle')


class FindVehicleByVIN(Model):
    class meta(object):
        tagname = 'FindVehicleByVIN'
        namespace = 'http://WebCatalog.Kito.ec'

    items = fields.List('Vehicle')


class ExecCustomOperation(Model):
    class meta(object):
        tagname = 'ExecCustomOperation'
        namespace = 'http://WebCatalog.Kito.ec'

    items = fields.List('Vehicle')


class WizardOption(Model):
    class meta(object):
        namespace = 'http://WebCatalog.Kito.ec'
        tagname = 'row'

    key = fields.String()
    value = fields.String()


class WizardParameter(Model):
    """
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:GetWizard2
    Получить возможные варианты параметров автомобиля.
    Данная функция доступна только при указании параметра
    supportparameteridentification2 в функциях ListCatalogs и GetCatalogInfo
    """

    class meta(object):
        namespace = 'http://WebCatalog.Kito.ec'
        tagname = 'row'

    # Признак того, что есть возможность получить список вариантов автомобилей,
    # подходящих под выбранные условия
    allowlistvehicles = fields.Boolean(default=False)

    # Наименование параметра
    name = fields.String()

    # Признак того, что параметр определен - выбран пользователем
    # или определился однозначно
    determined = fields.Boolean(default=False)

    # Признак того, что значение параметра автоматически определилось,
    # то есть остался один вариант значения
    automatic = fields.Boolean(default=False)

    # Перечень вариантов параметра
    options = fields.List('WizardOption', tagname='options')

    # Значение параметра, если он уже определен
    value = fields.String(default='')

    # Код типа параметра
    type = fields.String(default='')

    # SSD для отмены выбора параметра
    ssd = fields.String(default='')


class Wizard(Model):
    """
    Получить возможные варианты параметров автомобиля.
    Данная функция доступна только при указании параметра
    supportparameteridentification2 в функциях ListCatalogs и GetCatalogInfo
    """

    class meta(object):
        tagname = 'GetWizard2'
        namespace = 'http://WebCatalog.Kito.ec'

    parameters = fields.List('WizardParameter')

    @property
    def ready(self):
        """
        Признак того, что доступен список автомобилей под заданные параметры
        :return:
        """
        # todo test it
        return any((p.allowlistvehicles for p in self.parameters))


class Unit(Model):
    """
    Агрегат автомобиля
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:GetUnitInfo

    В силу того, что в ответе от сервиса поле note может присутствовать как
    в виде атрибута у тега <row>, так и в виде отдельного тега
    <attrbute key="note"/>, приходится прибегнуть к экзотическому способу
    получения значения в поле экземпляра класса
    """

    class meta(object):
        tagname = 'row'
        namespace = 'http://WebCatalog.Kito.ec'

    IMAGE_SIZE_FULL = 'source'
    IMAGE_SIZE_ORIGINAL = 'source'  # alias
    IMAGE_SIZE_150 = 150
    IMAGE_SIZE_175 = 175
    IMAGE_SIZE_200 = 200
    IMAGE_SIZE_225 = 225
    IMAGE_SIZE_250 = 250

    FLAG_NONSTANDARD_DETAIL = 0x01

    unitid = fields.String(default='')  # ID узла
    name = fields.String(default='')  # Наименование узла

    imageurl = fields.String(default='')  # URL изображения
    code = fields.String(default='')  # Код узла
    ssd = fields.String(default='')  # SSD

    # Показывает, что узел может быть не применим к данному автомобилю и
    # требуется уточнение параметров автомобиля.
    filter = fields.String(default='')  # Код условия
    flag = fields.String(default='')  # Флаги

    _note = fields.String(default='', attrname='note')  # Примечание к узлу
    _attributes = fields.Dict('Attribute', key="key")

    @property
    def note(self):
        assert isinstance(self._attributes, dict)
        try:
            attr = self._attributes['note']
            value = attr.value
        except (KeyError, AttributeError):
            value = self._note
        return value or ''


class UnitList(Model, IterableMixin):
    """
    Список агрегатов автомобиля
    Описание данных:
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListUnits
    """

    class meta(object):
        tagname = 'ListUnits'
        namespace = 'http://WebCatalog.Kito.ec'

    items = fields.List(Unit)


class Category(Model):
    """
    Категория каталога
    Описание данных:
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListCategories
    """

    class meta(object):
        tagname = 'row'
        namespace = 'http://WebCatalog.Kito.ec'

    categoryid = fields.String()  # Номер категории
    name = fields.String()  # Наименование категории
    code = fields.String(default='')  # Код категории
    childrens = fields.Boolean(default=False)  # Признак наличия вложенных кат.
    parentcategoryid = fields.String(default='')  # ID родительской категории
    ssd = fields.String()  # SSD


class CategoryList(Model, IterableMixin):
    """
    Список категорий каталога
    Описание данных:
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListCategories
    """

    class meta(object):
        tagname = 'ListCategories'
        namespace = 'http://WebCatalog.Kito.ec'

    items = fields.List(Category)


class FilterValue(Model):
    """
    Значения фильтра применимости узла к автомобилю
    Описание данных:
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:GetFilterByUnit
    """

    class meta(object):
        namespace = 'http://WebCatalog.Kito.ec'
        tagname = 'row'

    name = fields.String()  # Наименование значения
    note = fields.String()  # Примечание

    # Полученный SSDModification должен быть добавлен к концу
    # текущего ssd кода, а текущая страница перезагружена.
    ssdmodification = fields.String()  # Модификатор SSD


class Filter(Model):
    """
    Фильтр применимости узла к автомобилю
    Описание данных:
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:GetFilterByUnit
    """

    class meta(object):
        namespace = 'http://WebCatalog.Kito.ec'
        tagname = 'row'

    # Наименование условия
    name = fields.String()

    # list - Перечисляемый, input - Вводимый пользователем
    type = fields.String()

    # Список значений, если условие перечисляемое
    values = fields.List('FilterValue', tagname='values', required=False)

    # Регулярное выражение если значение вводится пользователем
    regexp = fields.String(default='')

    # Модификатор SSD для вводимых значений
    # Символ $ должен быть заменен на введенное пользователем
    # значение для условия.
    ssdmodification = fields.String(default='')


class UnitFilters(Model):
    """
    Набор фильтров применимости узла к автомобилю
    """

    class meta(object):
        namespace = 'http://WebCatalog.Kito.ec'
        tagname = 'GetFilterByUnit'

    items = fields.List('Filter')


class DetailFilters(Model):
    """
    Набор фильтров применимости узла к автомобилю
    """

    class meta(object):
        namespace = 'http://WebCatalog.Kito.ec'
        tagname = 'GetFilterByDetail'

    items = fields.List('Filter')


class Detail(Model):
    """
    Описывает деталь в агрегате автомобиля
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListDetailByUnit
    """

    class meta(object):
        tagname = 'row'
        namespace = 'http://WebCatalog.Kito.ec'

    FLAG_NONSTANDARD_DETAIL = 0x01

    # Код детали на иллюстрации для ListDetailByUnit.
    codeonimage = fields.String()  # Код детали
    oem = fields.String()  # OEM детали

    name = fields.String(default='')  # Наименование детали

    # Оригинальная цена детали в единицах региона
    price = fields.String(default='')  # Цена детали

    # Показывает что деталь может быть не применима к данному автомобилю
    # и требуется уточнение параметров автомобиля.
    filter = fields.String(default='')  # Код условия

    flag = fields.String(default='')  # Флаги

    # Используется в функции ListQuickDetail с параметром All,
    # показывает что данная деталь входит в искомую группу
    match = fields.String(default='')
    designation = fields.String(default='')
    applicablemodels = fields.String(default='')
    partspec = fields.String(default='')
    color = fields.String(default='')  # Код цвета
    shape = fields.String(default='')
    standard = fields.String(default='')
    material = fields.String(default='')
    size = fields.String(default='')
    featuredescription = fields.String(default='')
    prodstart = fields.String(default='')
    prodend = fields.String(default='')

    _note = fields.String(default='', attrname='note')
    _amount = fields.String(default='', attrname='amount')
    _attributes = fields.Dict('Attribute', key="key")

    @property
    def attributes(self):
        return self._attributes

    @property
    def note(self):
        assert isinstance(self._attributes, dict)
        try:
            attr = self._attributes['note']
            value = attr.value
        except (KeyError, AttributeError):
            value = self._note
        return value or ''

    @property
    def amount(self):
        assert isinstance(self._attributes, dict)
        try:
            attr = self._attributes['amount']
            value = attr.value
        except (KeyError, AttributeError):
            value = self._amount
        return value or ''


class DetailList(Model):
    """
    Описывает список деталей в агрегате автомобиля
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListDetailByUnit
    """

    class meta(object):
        tagname = 'ListDetailsByUnit'
        namespace = 'http://WebCatalog.Kito.ec'

    items = fields.List(Detail)


class ImageMap(Model):
    """
    Описывает координаты "горячих" областей на иллюстрации агрегата автомобиля.
    """

    class meta(object):
        tagname = 'row'
        namespace = 'http://WebCatalog.Kito.ec'

    x1 = fields.String()
    y1 = fields.String()
    x2 = fields.String()
    y2 = fields.String()
    type = fields.String()  # Тип ссылки

    # Код детали на картинке, соответствует коду детали в списке деталей узла
    code = fields.String()  # Код детали


class ImageMapList(Model):
    """
    Описывает координаты "горячих" областей на иллюстрации агрегата автомобиля.
    """

    class meta(object):
        tagname = 'ListImageMapByUnit'
        namespace = 'http://WebCatalog.Kito.ec'

    items = fields.List('ImageMap')


class QuickGroup(Model, IterableMixin):
    """
    Описывает группу быстрого поиска деталей для автомобиля
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListQuickGroup
    """

    class meta(object):
        tagname = 'row'
        namespace = 'http://WebCatalog.Kito.ec'

    quickgroupid = fields.String()  # ID группы
    name = fields.String()  # Наименование группы

    # Возможность поиска деталей в данной группе.
    # False означает, что веб-сервис не даст ни одной детали в результате.
    link = fields.Boolean(default=False)  # Возможность поиска деталей

    items = fields.List('QuickGroup')


class QuickGroupTree(Model, IterableMixin):
    """
    Вложенный список групп быстрого поиска деталей
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListQuickGroup
    """

    class meta(object):
        tagname = 'ListQuickGroups'
        namespace = 'http://WebCatalog.Kito.ec'

    items = fields.List('QuickGroup')


class QuickDetailTree(Model, IterableMixin):
    """
    Вложенный список деталей для группы быстрого поиска деталей для автомобиля
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListQuickDetail
    """

    class meta(object):
        tagname = 'ListQuickDetail'
        namespace = 'http://WebCatalog.Kito.ec'

    # Т.к. везде, кроме этого кейса в структуре XML тегом каждой сущности
    # является <row>, а тут они внезапно (!) обозначились по имени,
    # приходится переопределять классы для вложенных в QuickDetailTree
    # объектов, чтобы в данном случае оно парсилось по тегам Category, Unit,
    # Detail, etc.
    # Уродство, но что поделать, если они такие панки :/
    # todo: подумать, может есть решение получше
    class QDetail(Detail):
        class meta(object):
            tagname = 'Detail'

        @property
        def is_match(self):
            return self.match == 't'

    class QUnit(Unit, IterableMixin):
        class meta(object):
            tagname = 'Unit'

        details = fields.List('QDetail')
        items = details

        @cached_property
        def matched_codes(self):
            assert isinstance(self.details, list)  # typehint
            return [d.codeonimage for d in [d for d in self.details if d.is_match]]

    class QCategory(Category, IterableMixin):
        class meta(object):
            tagname = 'Category'

        units = fields.List('QUnit')
        items = units

    categories = fields.List('QCategory')
    items = categories
