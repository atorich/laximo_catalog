# coding: utf-8




from laximo import settings
from laximo.oem import objects
from laximo.query import Query


class BaseOEMQuery(Query):
    def __init__(self, ssd=None, locale=None, **kwargs):
        if locale is None:
            locale = settings.LOCALE

        if ssd is None:
            ssd = ''

        kwargs.update({
            'ssd': ssd,
            'Locale': locale,
        })

        super(BaseOEMQuery, self).__init__(kwargs)


class ListCatalogs(BaseOEMQuery):
    """
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListCatalogs
    """
    name = 'ListCatalogs'

    class Meta(object):
        selector = '//response/ListCatalogs'
        object_cls = objects.CatalogList


class GetCatalogInfo(BaseOEMQuery):
    """
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:GetCatalogInfo
    """
    name = 'GetCatalogInfo'

    class Meta(object):
        selector = '//response/GetCatalogInfo/row'
        object_cls = objects.CatalogInfo

    def __init__(self, catalog, **params):
        super(GetCatalogInfo, self).__init__(Catalog=catalog, **params)


class GetVehicleInfo(BaseOEMQuery):
    """
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:GetVehicleInfo
    """
    name = 'GetVehicleInfo'

    class Meta(object):
        selector = '//response/GetVehicleInfo/row'
        object_cls = objects.Vehicle

    def __init__(self, catalog, vehicle_id, **params):
        super(GetVehicleInfo, self).__init__(
            Catalog=catalog, VehicleId=vehicle_id, Localized=True, **params
        )


class FindVehicleByVIN(BaseOEMQuery):
    """
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:FindVehicleByVIN
    """
    name = 'FindVehicleByVIN'

    class Meta(object):
        selector = '//response/FindVehicleByVIN'
        object_cls = objects.FindVehicleByVIN

    def __init__(self, vin, catalog=None, **params):
        # Хоть аргумент помечен в документации как необязательный, но
        # если его не передавать вообще, возвращается ошибка, поэтому
        # при отсутствии аргумента будем передавать туда пустую строку
        if not catalog:
            catalog = ''

        super(FindVehicleByVIN, self).__init__(
            VIN=vin, Catalog=catalog, Localized=True, **params
        )


class FindVehicleByFrame(BaseOEMQuery):
    """
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:FindVehicleByVIN
    """
    name = 'FindVehicleByFrame'

    class Meta(object):
        selector = '//response/FindVehicleByFrame'
        object_cls = objects.FindVehicleByFrame

    def __init__(self, frame, frameno, catalog='', **params):
        super(FindVehicleByFrame, self).__init__(
            Frame=frame, FrameNo=frameno, Catalog=catalog, **params
        )


class FindVehicleByWizard(BaseOEMQuery):
    name = 'FindVehicleByWizard2'

    class Meta(object):
        selector = '//response/FindVehicleByWizard2'
        object_cls = objects.FindVehicleByWizard

    def __init__(self, catalog, **params):
        super(FindVehicleByWizard, self).__init__(Catalog=catalog, **params)


class GetWizard(BaseOEMQuery):
    """
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:GetWizard2
    """
    name = 'GetWizard2'

    class Meta(object):
        selector = '//response/GetWizard2'
        object_cls = objects.Wizard

    def __init__(self, catalog, **params):
        super(GetWizard, self).__init__(Catalog=catalog, **params)


class ExecCustomOperation(BaseOEMQuery):
    """
    http://technologytrade.ru/index.php/:Laximo_Web-services:OEM:ExecCustomOperation
    """
    name = 'ExecCustomOperation'

    class Meta(object):
        selector = '//response/ExecCustomOperation'
        object_cls = objects.ExecCustomOperation

    def __init__(self, catalog, operation, **params):
        super(ExecCustomOperation, self).__init__(
            Catalog=catalog, operation=operation, **params
        )


class ListUnits(BaseOEMQuery):
    """
    http://technologytrade.ru/index.php/:Laximo_Web-services:OEM:ListUnits
    """
    name = 'ListUnits'

    class Meta(object):
        selector = '//response/ListUnits'
        object_cls = objects.UnitList

    def __init__(self, catalog, vehicle_id, category_id=None, **params):
        super(ListUnits, self).__init__(
            Catalog=catalog, VehicleId=vehicle_id, CategoryId=category_id,
            Localized=True, **params
        )


class GetUnitInfo(BaseOEMQuery):
    """
    http://technologytrade.ru/index.php/:Laximo_Web-services:OEM:GetUnitInfo
    """
    name = 'GetUnitInfo'

    class Meta(object):
        selector = '//response/GetUnitInfo/row'
        object_cls = objects.Unit

    def __init__(self, catalog, unit_id, **params):
        # http://technologytrade.ru/index.php/Laximo_Web-services:OEM:GetUnitInfo
        # В документации, по идее, unit_id не я вляется обязательным полем,
        # но по логике таковым быть должно
        super(GetUnitInfo, self).__init__(
            Catalog=catalog, UnitId=unit_id, Localized=True, **params
        )


class ListCategories(BaseOEMQuery):
    """
    http://technologytrade.ru/index.php/:Laximo_Web-services:OEM:ListCategories
    """
    name = 'ListCategories'

    class Meta(object):
        selector = '//response/ListCategories'
        object_cls = objects.CategoryList

    def __init__(self, catalog, vehicle_id, category_id=-1, **params):
        super(ListCategories, self).__init__(
            Catalog=catalog, VehicleId=vehicle_id, CategoryId=category_id,
            **params
        )


class GetFilterByUnit(BaseOEMQuery):
    """
    http://technologytrade.ru/index.php/:Laximo_Web-services:OEM:GetFilterByUnit
    """
    name = 'GetFilterByUnit'

    class Meta(object):
        selector = '//response/GetFilterByUnit'
        object_cls = objects.UnitFilters

    def __init__(self, catalog, unit_id, vehicle_id, filter, **params):
        super(GetFilterByUnit, self).__init__(
            Catalog=catalog,
            UnitId=unit_id,
            VehicleId=vehicle_id,
            Filter=filter,
            **params
        )


class ListDetailByUnit(BaseOEMQuery):
    """
    http://technologytrade.ru/index.php/:Laximo_Web-services:OEM:ListDetailByUnit
    """
    name = 'ListDetailByUnit'

    class Meta(object):
        selector = '//response/ListDetailsByUnit'
        object_cls = objects.DetailList

    def __init__(self, catalog, unit_id, **params):
        super(ListDetailByUnit, self).__init__(
            Catalog=catalog,
            UnitId=unit_id,
            Localized=True,
            **params
        )


class ListImageMapByUnit(BaseOEMQuery):
    """
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListImageMapByUnit
    """
    name = 'ListImageMapByUnit'

    class Meta(object):
        selector = '//response/ListImageMapByUnit'
        object_cls = objects.ImageMapList

    def __init__(self, catalog, unit_id, **params):
        super(ListImageMapByUnit, self).__init__(
            Catalog=catalog,
            UnitId=unit_id,
            **params
        )


class GetFilterByDetail(BaseOEMQuery):
    """
    Получить список условий, которые необходимы для определения применимости
    детали к автомобилю.
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:GetFilterByDetail
    """
    name = 'GetFilterByDetail'

    class Meta(object):
        selector = '//response/GetFilterByDetail'
        object_cls = objects.DetailFilters

    def __init__(self, catalog, unit_id, detail_id, filter, **params):
        """

        :param catalog: Код каталога
        :param unit_id: Идентификатор узла
        :param detail_id: Код детали
        :param filter: Код условия, полученного в функции ListUnits
        :param params:
        """
        super(GetFilterByDetail, self).__init__(
            Catalog=catalog, UnitId=unit_id, DetailId=detail_id, Filter=filter,
            **params
        )


class ListQuickGroup(BaseOEMQuery):
    """
    Список групп быстрого поиска деталей для автомобиля
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListQuickGroup
    """
    name = 'ListQuickGroup'

    class Meta(object):
        selector = '//response/ListQuickGroups'
        object_cls = objects.QuickGroupTree

    def __init__(self, catalog, vehicle_id, **params):
        super(ListQuickGroup, self).__init__(
            Catalog=catalog, VehicleId=vehicle_id, **params
        )


class ListQuickDetail(BaseOEMQuery):
    """
    Список деталей для группы быстрого поиска деталей для автомобиля.
    http://technologytrade.ru/index.php/Laximo_Web-services:OEM:ListQuickDetail

    :param catalog: Код каталога, берется из списка каталогов
    :param vehicle_id: Идентификатор автомобиля
    :param quick_group_id: Идентификатор группы быстрого поиска
    :param all: Если установлен, сервис вернет все позиции найденного узла.
    Подходящие к группе позиции помечаются в этом случае специальным тегом.
    """
    name = 'ListQuickDetail'

    class Meta(object):
        selector = '//response/ListQuickDetail'
        object_cls = objects.QuickDetailTree

    def __init__(self, catalog, vehicle_id, quick_group_id, all=1, **params):
        super(ListQuickDetail, self).__init__(
            Catalog=catalog, VehicleId=vehicle_id, QuickGroupId=quick_group_id,
            All=all, Localized=True, **params
        )
