# coding=utf-8


import logging
import time


import requests
from lxml.etree import XMLSyntaxError
from requests import RequestException
from suds.client import Client
from suds.transport import Reply, TransportError
from suds.transport.http import HttpAuthenticated

from laximo import __version__, settings
from laximo.exceptions import MalformedResponse, LaximoRequestException
from laximo.query import QuerySet
from laximo.response import Response

logger = logging.getLogger(__name__)


class RequestTransport(HttpAuthenticated):
    """Транспорт запроса для certificate-based suds клиента"""
    def __init__(self, **kwargs):
        self.soap_action = settings.OEM_SOAP_ACTION
        HttpAuthenticated.__init__(self, **kwargs)

    def send(self, request):
        self.addcredentials(request)

        request = self.create_request(
            url=request.url, data=request.message
        )
        prepared = request.prepare()
        http_params = self.build_http_params()

        with requests.Session() as session:
            resp = session.send(prepared, **http_params)

        result = Reply(resp.status_code, resp.headers, resp.content)
        return result

    def build_headers(self, headers=None):
        headers = headers or {}
        headers.setdefault(
            'User-Agent', 'LaximoPy [#{version}]'.format(version=__version__))
        headers.setdefault('Accept', '*/*')
        headers.setdefault('Content-Type', "text/xml; charset=UTF-8")
        headers.setdefault('SOAPAction', self.soap_action)
        return headers

    @staticmethod
    def build_http_params(http_params=None):
        http_params = http_params or {}
        http_params.setdefault('verify', settings.SSL_VERIFY)
        http_params.setdefault('cert', settings.SSL_CERT)
        http_params.setdefault(
            'timeout', (settings.CONNECT_TIMEOUT, settings.READ_TIMEOUT))
        return http_params

    def create_request(self, **kwargs):
        headers = self.build_headers(kwargs.get('headers', {}))
        return requests.Request(
            'POST', headers=headers, **kwargs
        )


class BaseRequest(object):
    wsdl = None  # set in subclasses

    response_cls = Response
    client_cls = Client
    transport_cls = RequestTransport

    def __init__(self, queryset=None):
        transport = self.create_transport()
        self._response = None
        self.client = self.client_cls(
            self.wsdl, transport=transport,
            retxml=True
        )

        if queryset is None:
            queryset = QuerySet()

        self.queryset = queryset

    def create_transport(self):
        return self.transport_cls()

    def _call(self):
        q = str(self.queryset)

        logger.debug('Call QueryData with:\n%s' % q)
        t1 = time.time()
        resp = self.client.service.QueryData(q)
        t2 = time.time()
        logger.debug('Request time: %ss' % (t2 - t1))

        return resp

    def call(self):
        try:
            data = self._call()
        except (RequestException, TransportError) as e:
            # RequestException - общий родитель для ReadTimeout и ConnectionError
            message = "%s: %s" % (type(e), e.message)
            raise LaximoRequestException(message)

        try:
            response = self.response_cls(self, data)
        except XMLSyntaxError:
            raise MalformedResponse('Malformed response: %s' % data)

        if response.is_fault:
            logger.error('Laximo fault:\n%s' % response.fault.message)
        else:
            logger.debug('%s' % response.data)

        return response

    @property
    def response(self):
        if not self._response:
            self._response = self.call()
        return self._response

    def __str__(self):
        return str(self.response)


class OEMRequest(BaseRequest):
    wsdl = settings.OEM_SOAP_WSDL


class AftermarketRequest(BaseRequest):
    wsdl = settings.AFTERMARKET_SOAP_WSDL
