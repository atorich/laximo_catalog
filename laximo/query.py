# coding=utf-8






class Query(object):
    """
    Собирает текстовый запрос в сервис
    http://technologytrade.ru/index.php/Laximo_Web-services/Work_principles
    """
    name = None

    def __init__(self, params):
        self.params = params

    def prepare_params(self):
        params = ["{}={}".format(*param) for param in iter(list(self.params.items()))]
        return "|".join(params)

    def __str__(self):
        tmpl = "{name}:{params}"
        params = self.prepare_params()
        return tmpl.format(name=self.name, params=params)


class QuerySet(object):

    def __init__(self, *queries):
        self._queries = list(queries)

    def add_query(self, query):
        self._queries.append(query)

    def __getitem__(self, item):
        return self._queries[item]

    def __str__(self):
        return '\n'.join(str(q) for q in self._queries)

    def __iter__(self):
        return iter(self._queries)

    def __bool__(self):
        return len(self._queries) > 0
