

import re


def xpath_ns(tree, expr):
    """
    http://stackoverflow.com/questions/5572247/how-to-find-xml-elements-via-xpath-in-python-in-a-namespace-agnostic-way
    Parse a simple expression and prepend namespace wildcards where unspecified
    """
    if expr is None:
        return []

    def qual(n):
        return n if not n or ':' in n else '*[local-name() = "%s"]' % n

    expr = '/'.join(qual(n) for n in expr.split('/'))
    nsmap = dict((k, v) for k, v in list(tree.nsmap.items()) if k)
    return tree.xpath(expr, namespaces=nsmap)


def slugify(value):
    slug = re.sub(r'(\-+)|(\/+)|(_+)', ' ', value)
    slug = re.sub(r'\s+', ' ', slug)
    return slug.lower().replace(' ', '-').strip()
