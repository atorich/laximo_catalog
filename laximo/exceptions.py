# coding: utf-8



class LaximoException(Exception):
    """
    Базовое исключение для пакета Laximo
    При обертывании стандартных и 3rd-party исключений
    есть возможность указать ссылку на оригинальное через
    параметр ```original_exc```
    """

    def __init__(self, *args, **kwargs):
        self.original_exc = kwargs.pop('original_exc', None)
        super(LaximoException, self).__init__(*args, **kwargs)


class MalformedResponse(LaximoException):
    pass


class LaximoRequestException(LaximoException):
    pass
