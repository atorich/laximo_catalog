

E_INVALIDREQUEST = 'E_INVALIDREQUEST'
E_INVALIDPARAMETER = 'E_INVALIDPARAMETER'
E_CATALOGNOTEXISTS = 'E_CATALOGNOTEXISTS'
E_UNKNOWNCOMMAND = 'E_UNKNOWNCOMMAND'
E_ACCESSDENIED = 'E_ACCESSDENIED'
E_NOTSUPPORTED = 'E_NOTSUPPORTED'
E_GROUP_IS_NOT_SEARCHABLE = 'E_GROUP_IS_NOT_SEARCHABLE'
E_UNEXPECTED_PROBLEM = 'E_UNEXPECTED_PROBLEM'

ERROR_MESSAGES = {
    E_INVALIDPARAMETER: "Invalid parameter: {}",
    E_CATALOGNOTEXISTS: "Catalog {} not exists",
    E_UNEXPECTED_PROBLEM: "Unexpected problem ({})",
}
