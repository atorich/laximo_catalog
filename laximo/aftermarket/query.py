# coding: utf-8




from laximo import settings
from laximo.aftermarket import objects
from laximo.query import Query


# Опции поиска
# http://technologytrade.ru/index.php/Laximo_Web-services:AM:Search_options


def combine_options(*opts):
    return ','.join((o.strip() for o in opts))


class SearchOption(str):
    """
    Обертка над строкой, позволяющая комбинировать опции через оператор |

    Например:
        SEARCH_OPTION_CROSSES | SEARCH_OPTION_PRICES == 'crosses,prices'
    """

    def __or__(self, other):
        return SearchOption(','.join((self, other)))


SEARCH_OPTION_CROSSES = SearchOption('crosses')
SEARCH_OPTION_PRICES = SearchOption('prices')
SEARCH_OPTION_WEIGHTS = SearchOption('weights')
SEARCH_OPTION_NAMES = SearchOption('names')
SEARCH_OPTION_PROPERTIES = SearchOption('properties')
SEARCH_OPTION_IMAGES = SearchOption('images')


class BaseAMQuery(Query):
    def __init__(self, locale=None, **kwargs):
        if locale is None:
            locale = settings.LOCALE

        kwargs.update({
            'Locale': locale,
        })

        super(BaseAMQuery, self).__init__(kwargs)


class FindOEM(BaseAMQuery):
    """
    http: // technologytrade.ru / index.php / Laximo_Web - services:AM:FindOEM
    """
    name = 'FindOEM'

    class Meta(object):
        selector = '//response/FindOEM'
        object_cls = objects.FindOEM

    def __init__(self, oem, options, brand=None, **params):
        super(FindOEM, self).__init__(
            OEM=oem, Options=options, brand=brand, **params
        )


class FindReplacements(BaseAMQuery):
    name = 'FindReplacements'

    class Meta(object):
        selector = '//response/FindReplacements'
        object_cls = objects.FindReplacements

    def __init__(self, detail_id, **params):
        super(FindReplacements, self).__init__(
            DetailId=detail_id, **params
        )


class FindDetails(BaseAMQuery):
    name = 'FindDetail'

    class Meta(object):
        selector = '//response/FindDetails'
        object_cls = objects.FindDetails

    def __init__(self, detail_id, options, **params):
        super(FindDetails, self).__init__(
            DetailId=detail_id, Options=options, **params
        )


class FindOEMCorrection(BaseAMQuery):
    name = 'FindOEMCorrection'

    class Meta(object):
        selector = '//response/FindOEMCorrection'
        object_cls = objects.OEMCorrection

    def __init__(self, oem, brand=None, **params):
        super(FindOEMCorrection, self).__init__(OEM=oem, brand=brand, **params)


class ManufacturerInfo(BaseAMQuery):
    name = 'ManufacturerInfo'

    class Meta(object):
        selector = '//response/ManufacturerInfo/row'
        object_cls = objects.ManufacturerInfo

    def __init__(self, manufacturer_id, **params):
        super(ManufacturerInfo, self).__init__(
            ManufacturerId=manufacturer_id, **params)


class ListManufacturer(BaseAMQuery):
    name = 'ListManufacturer'

    class Meta(object):
        selector = '//response/ListManufacturer'
        object_cls = objects.ListManufacturer

    def __init__(self, *args, **kwargs):
        super(ListManufacturer, self).__init__(*args, **kwargs)
