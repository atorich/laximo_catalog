# coding: utf-8



from dexml import Model, fields

from laximo.objects import IterableMixin, LabeledString


class DetailProperty(Model):
    class meta(object):
        tagname = 'property'
        namespace = 'http://Aftermarket.Kito.ec'

    property = LabeledString(default='', verbose_name='Наименование свойства')
    code = LabeledString(default='', verbose_name='Код свойства')
    value = LabeledString(default='', verbose_name='Значение свойства')
    locale = LabeledString(default='', verbose_name='Язык свойства')
    detailpropertyid = LabeledString(default='', verbose_name='ID свойства')
    rate = LabeledString(default='', verbose_name='Рейтинг свойства')


class DetailImage(Model):
    class meta(object):
        tagname = 'image'
        namespace = 'http://Aftermarket.Kito.ec'

    filename = LabeledString(default='', verbose_name='URL картинки')
    width = LabeledString(default='', verbose_name='Ширина')
    height = LabeledString(default='', verbose_name='Высота')


class DetailReplacement(Model):
    class meta(object):
        tagname = 'replacement'
        namespace = 'http://Aftermarket.Kito.ec'

    type = LabeledString(verbose_name='Тип замены')
    way = LabeledString(verbose_name='Направление замены')
    replacementid = LabeledString(verbose_name='ID замены', default='')
    rate = fields.Float(verbose_name='Рейтинг', default=0.0)
    detail = fields.Model('detail', required=False)


class Detail(Model):
    class meta(object):
        tagname = 'detail'
        namespace = 'http://Aftermarket.Kito.ec'

    detailid = LabeledString(
        default='', verbose_name='ID детали')
    manufacturerid = LabeledString(
        default='', verbose_name='Идентификатор производителя')
    manufacturer = LabeledString(
        default='', verbose_name='Код производителя')
    oem = LabeledString(
        default='', verbose_name='Артикул')
    formattedoem = LabeledString(
        default='', verbose_name='Форматированный артикул')
    name = LabeledString(
        default='', verbose_name='Наименование')
    weight = LabeledString(
        default='', verbose_name='Вес, кг.')
    volume = LabeledString(
        default='', verbose_name='Объем, м. куб.')
    dimensions = LabeledString(
        default='', verbose_name='Размеры, м.'
    )

    images = fields.List(
        'DetailImage', tagname='images', required=False)
    properties = fields.List(
        'DetailProperty', tagname='properties', required=False)
    replacements = fields.List(
        'DetailReplacement', tagname='replacements', required=False)


class FindOEM(Model, IterableMixin):
    """
    Список деталей, найденных по OEM
    Описание данных:
    http://technologytrade.ru/index.php/Laximo_Web-services:AM:FindOEM
    """

    class meta(object):
        tagname = 'FindOEM'
        namespace = 'http://Aftermarket.Kito.ec'

    items = fields.List('detail')


class Replacement(Model):
    """
    Описывает объект замены
    http://technologytrade.ru/index.php/Laximo_Web-services:AM:FindReplacements
    """

    class meta(object):
        tagname = 'row'
        namespace = 'http://Aftermarket.Kito.ec'

    type = LabeledString(default='', verbose_name='Тип связи')
    rate = LabeledString(default='', verbose_name='Рейтинг')
    detailid = LabeledString(default='', verbose_name='ID детали')
    manufacturerid = LabeledString(
        default='', verbose_name='Идентификатор производителя')
    manufacturer = LabeledString(
        default='', verbose_name='Код производителя')
    searchurl = LabeledString(
        default='', verbose_name='URL поиска на сайте производителя')
    oem = LabeledString(default='', verbose_name='Артикул OEM')
    formattedoem = LabeledString(
        default='', verbose_name='Форматированные артикул ОЕМ')
    name = LabeledString(default='', verbose_name='Наименование')
    weight = LabeledString(default='', verbose_name='Вес в килограммах')

    # далее какая-то недокументировнная, но присутствующая в ответе хрень
    type1 = LabeledString(default='')
    type2 = LabeledString(default='')
    replacementtype1 = LabeledString(default='')
    replacementtype2 = LabeledString(default='')


class FindReplacements(Model, IterableMixin):
    """
    Опиывает ответ на запрос поиска по кроссам второго уровня.
    http://technologytrade.ru/index.php/Laximo_Web-services:AM:FindReplacements
    """

    class meta(object):
        tagname = 'FindReplacements'
        namespace = 'http://Aftermarket.Kito.ec'

    items = fields.List(Replacement)


class FindDetails(Model, IterableMixin):
    """
    Отписывает ответ на запрос поиска деталей
    http://technologytrade.ru/index.php/Laximo_Web-services:AM:FindDetail
    """

    class meta(object):
        tagname = 'FindDetails'
        namespace = 'http://Aftermarket.Kito.ec'

    items = fields.List('detail')


class OEMCorrection(Model, IterableMixin):
    class meta(object):
        tagname = 'FindOEMCorrection'
        namespace = 'http://Aftermarket.Kito.ec'

    items = fields.List('detail')


class ManufacturerInfo(Model):
    class meta(object):
        tagname = 'row'
        namespace = 'http://Aftermarket.Kito.ec'

    manufacturerid = LabeledString(verbose_name='ID производителя')
    code = LabeledString(default='', verbose_name='Код производителя')
    name = LabeledString(verbose_name='Наименование')
    isoriginal = fields.Boolean(default=False)
    searchurl = LabeledString(
        default='', verbose_name='URL для поиска на сайте производителя')
    alias = LabeledString(default='', verbose_name='Alias')


class ListManufacturer(Model, IterableMixin):
    class meta(object):
        tagname = 'ListManufacturer'
        namespace = 'http://Aftermarket.Kito.ec'

    items = fields.List('ManufacturerInfo')
