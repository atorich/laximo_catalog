import os


SSL_VERIFY = False

SSL_CERT = (
    os.environ.get('LAXIMO_PEM_KEY', '/path/to/laximo.pem'),  # public key
    os.environ.get('LAXIMO_PRIVATE_KEY', '/path/to/laximo.key')  # private key
)

CONNECT_TIMEOUT = 10
READ_TIMEOUT = 25

OEM_SOAP_WSDL = 'http://ws.laximo.net/wsdl/Laximo.OEM_Certificate.xml'
OEM_SOAP_ACTION = 'http://WebCatalog.Kito.ec'

AFTERMARKET_SOAP_WSDL = \
    'http://ws.laximo.net/wsdl/Laximo.Aftermarket_Certificate.xml'

LOCALE = 'en_EU'

UNESCAPE_RESPONSE_DATA = True
